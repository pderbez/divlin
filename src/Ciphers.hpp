#ifndef CIPHERS_HPP
#define CIPHERS_HPP

#include <vector>
#include "DivTable.hpp"


class SearchParam
{
public:
  SearchParam(std::vector<std::vector<DivTable>> v, std::vector<std::vector<std::pair<std::vector<uint16_t>, unsigned>>> si,
    std::vector<std::vector<std::pair<std::vector<uint16_t>, unsigned>>> so, std::vector<uint8_t> p, std::vector<uint8_t> p_inv) : vdiv(move(v)),
    s_in (move(si)), s_out (move(so)), perm (move(p)), perm_inv (move(p_inv)) {};

  std::vector<std::vector<DivTable>> vdiv;
  std::vector<std::vector<std::pair<std::vector<uint16_t>, unsigned>>> s_in;
  std::vector<std::vector<std::pair<std::vector<uint16_t>, unsigned>>> s_out;
  std::vector<uint8_t> perm;
  std::vector<uint8_t> perm_inv;
};

SearchParam Midori(unsigned R);
SearchParam Skinny(unsigned R);
SearchParam Present(unsigned R);
SearchParam Gift (unsigned R);
SearchParam Twine (unsigned R);
SearchParam Led(unsigned R);
SearchParam Hight(unsigned R);
SearchParam Craft(unsigned R);
SearchParam Joltik(unsigned R);

#endif
