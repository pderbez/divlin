#include "SearchMon.hpp"
#include <execution>
#include <map>

using namespace std;

map<pair<set<uint8_t>, set<uint8_t>>, pair<vector<uint8_t>, vector<uint8_t>>>
makeTableK() {
  vector<uint8_t> v;
  vector<uint8_t> v0{0};
  vector<uint8_t> v1{1};
  vector<uint8_t> v01{0, 1};

  set<uint8_t> s;
  set<uint8_t> s0{0};
  set<uint8_t> s1{1};
  set<uint8_t> s01{0, 1};

  map<pair<set<uint8_t>, set<uint8_t>>, pair<vector<uint8_t>, vector<uint8_t>>>
      res;

  res.emplace(make_pair(s0, s), make_pair(v01, v));
  res.emplace(make_pair(s1, s), make_pair(v1, v));
  res.emplace(make_pair(s, s0), make_pair(v1, v0));
  res.emplace(make_pair(s, s1), make_pair(v, v1));
  res.emplace(make_pair(s01, s), make_pair(v01, v));
  res.emplace(make_pair(s, s01), make_pair(v1, v0));
  res.emplace(make_pair(s0, s1), make_pair(v01, v));
  res.emplace(make_pair(s1, s0), make_pair(v1, v0));

  return res;
}

pair<vector<uint8_t>, vector<uint8_t>>
computeTransform(pair<set<uint8_t>, set<uint8_t>> const &p,
                 vector<Polynome> const &pol) {
  vector<uint8_t> sure;
  vector<uint8_t> maybe;
  vector<uint8_t> inter(p.first.size() + p.second.size());
  for (unsigned u = 0; u < pol.size(); ++u) {
    auto it = set_intersection(pol[u].begin(), pol[u].end(), p.first.begin(),
                               p.first.end(), inter.begin());
    if (it - inter.begin() != 0)
      maybe.emplace_back(u);
    else {
      it = set_intersection(pol[u].begin(), pol[u].end(), p.second.begin(),
                            p.second.end(), inter.begin());
      if ((it - inter.begin()) % 2 != 0)
        sure.emplace_back(u);
    }
  }
  //maybe.clear();
  return make_pair(move(maybe), move(sure));
}

map<pair<set<uint8_t>, set<uint8_t>>, pair<vector<uint8_t>, vector<uint8_t>>>
makeTable(vector<Polynome> const &pol, unsigned n) {
  cout << n << endl;
  vector<pair<vector<uint8_t>, vector<uint8_t>>> tmp(1);
  for (unsigned i = 0; i < n; ++i) {
    vector<pair<vector<uint8_t>, vector<uint8_t>>> tmp2;
    for (auto &x : tmp) {
      auto x0 = x;
      x0.first.emplace_back(i);
      tmp2.emplace_back(move(x0));
      auto x1 = x;
      x1.second.emplace_back(i);
      tmp2.emplace_back(move(x1));
      tmp2.emplace_back(move(x));
    }
    tmp = move(tmp2);
    cout << tmp.size() << endl;
  }

  map<pair<set<uint8_t>, set<uint8_t>>, pair<vector<uint8_t>, vector<uint8_t>>>
      res;

  for (auto &x : tmp) {
    vector<uint8_t> sure;
    vector<uint8_t> maybe;
    vector<uint8_t> inter(n);
    for (unsigned u = 0; u < pol.size(); ++u) {
      auto it = set_intersection(pol[u].begin(), pol[u].end(), x.first.begin(),
                                 x.first.end(), inter.begin());
      if (it - inter.begin() != 0)
        maybe.emplace_back(u);
      else {
        it = set_intersection(pol[u].begin(), pol[u].end(), x.second.begin(),
                              x.second.end(), inter.begin());
        if ((it - inter.begin()) % 2 != 0)
          sure.emplace_back(u);
      }
    }
    res.emplace(make_pair(set<uint8_t>(x.first.begin(), x.first.end()),
                          set<uint8_t>(x.second.begin(), x.second.end())),
                make_pair(move(maybe), move(sure)));
  }
  return res;
}

SearchMon::SearchMon(vector<uint8_t> p, vector<uint16_t> const &sbox,
                     vector<vector<uint8_t>> const &m)
    : perm(move(p)), m_size(m.size()) {

  auto anf_s = getANF_S(sbox);
  for (unsigned u = 0; u < 1u << anf_s.size(); ++u) {
    Polynome res(0); // res = 1
    for (unsigned i = 0; i < anf_s.size(); ++i) {
      if (((u >> i) & 1) != 0)
        res *= anf_s[i];
    }
    pol_sbox.emplace_back(move(res));
  }

  // table_s = makeTable(pol_sbox, 1u << anf_s.size());

  s_size = anf_s.size();

  vector<Polynome> anf_m;
  for (unsigned l = 0; l < m.size(); ++l) {
    Polynome res; // res = 0;
    for (unsigned c = 0; c < m[l].size(); ++c) {
      if (m[l][c] != 0)
        res += Monome(1u << c);
    }
    anf_m.emplace_back(move(res));
  }

  for (unsigned u = 0; u < 1u << anf_m.size(); ++u) {
    Polynome res(0); // res = 1
    for (unsigned i = 0; i < anf_m.size(); ++i) {
      if (((u >> i) & 1) != 0)
        res *= anf_m[i];
    }
    pol_linear.emplace_back(move(res));
  }

  // table_m = makeTable(pol_linear, 1u << anf_m.size());

  table_k = makeTableK();
}

void SearchMon::applyK(unsigned b) {
  uint64_t mask_s = uint64_t(1) << b;
  uint64_t umask_s = ~mask_s;
  map<uint64_t, pair<set<uint8_t>, set<uint8_t>>> my_map;
  for (auto x : maybe)
    my_map[x & umask_s].first.emplace((x >> b) & 1);
  maybe.clear();
  for (auto x : sure)
    my_map[x & umask_s].second.emplace((x >> b) & 1);
  sure.clear();

  for (auto const &x : my_map) {
    auto const &my_pair = table_k.at(x.second);
    for (uint64_t y : my_pair.first)
      maybe.emplace_back(x.first | (y << b));
    for (uint64_t y : my_pair.second)
      sure.emplace_back(x.first | (y << b));
  }
  //cout << maybe.size() << " - " << sure.size() << endl;
}

void SearchMon::applyS(unsigned i) {
  uint64_t mask_s = 0;
  for (unsigned j = 0; j < s_size; ++j)
    mask_s |= uint64_t(1) << j;
  mask_s <<= s_size * i;
  uint64_t umask_s = ~mask_s;
  map<uint64_t, pair<set<uint8_t>, set<uint8_t>>> my_map;
  for (auto x : maybe)
    my_map[x & umask_s].first.emplace((x & mask_s) >> (s_size * i));
  maybe.clear();
  for (auto x : sure)
    my_map[x & umask_s].second.emplace((x & mask_s) >> (s_size * i));
  sure.clear();

  for (auto const &x : my_map) {
    auto it = table_s.find(x.second);
    if (it == table_s.end())
      it =
          table_s.emplace(x.second, computeTransform(x.second, pol_sbox)).first;
    auto const &my_pair = it->second;
    for (uint64_t y : my_pair.first)
      maybe.emplace_back(x.first | (y << (s_size * i)));
    for (uint64_t y : my_pair.second)
      sure.emplace_back(x.first | (y << (s_size * i)));
  }
  //cout << maybe.size() << " - " << sure.size() << endl;
}

void SearchMon::applyL(unsigned i) {
  uint64_t mask_s = 0;
  for (unsigned j = 0; j < m_size; ++j)
    mask_s |= uint64_t(1) << (i + j * s_size);
  uint64_t umask_s = ~mask_s;
  map<uint64_t, pair<set<uint8_t>, set<uint8_t>>> my_map;
  for (auto x : maybe) {
    uint8_t y = 0;
    for (unsigned j = 0; j < m_size; ++j)
      y |= ((x >> (i + j * s_size)) & 1) << j;
    my_map[x & umask_s].first.emplace(y);
  }
  maybe.clear();
  for (auto x : sure) {
    uint8_t y = 0;
    for (unsigned j = 0; j < m_size; ++j)
      y |= ((x >> (i + j * s_size)) & 1) << j;
    my_map[x & umask_s].second.emplace(y);
  }
  sure.clear();

  for (auto const &x : my_map) {
    auto it = table_m.find(x.second);
    if (it == table_m.end())
      it = table_m.emplace(x.second, computeTransform(x.second, pol_linear))
               .first;
    auto const &my_pair = it->second;
    for (uint64_t y : my_pair.first) {
      uint64_t z = 0;
      for (unsigned j = 0; j < m_size; ++j)
        z |= ((y >> j) & 1) << (i + j * s_size);
      maybe.emplace_back(x.first | z);
    }
    for (uint64_t y : my_pair.second) {
      uint64_t z = 0;
      for (unsigned j = 0; j < m_size; ++j)
        z |= ((y >> j) & 1) << (i + j * s_size);
      sure.emplace_back(x.first | z);
    }
  }
  //cout << maybe.size() << " - " << sure.size() << endl;
}

void SearchMon::applyL2(unsigned i) {
  uint64_t mask_s = 0;
  for (unsigned j = 0; j < m_size; ++j)
    mask_s |= uint64_t(1) << (i + j * s_size);
  uint64_t umask_s = ~mask_s;
  map<uint64_t, pair<set<uint8_t>, set<uint8_t>>> my_map;
  for (auto x : maybe) {
    uint8_t y = 0;
    for (unsigned j = 0; j < m_size; ++j)
      y |= ((x >> (i + j * s_size)) & 1) << j;
    my_map[x & umask_s].first.emplace(y);
  }
  maybe.clear();
  for (auto x : sure) {
    uint8_t y = 0;
    for (unsigned j = 0; j < m_size; ++j)
      y |= ((x >> (i + j * s_size)) & 1) << j;
    my_map[x & umask_s].second.emplace(y);
  }
  sure.clear();

  for (auto const &x : my_map) {
    auto it = table_m.find(x.second);
    if (it == table_m.end()) {
      auto tmp = computeTransform(x.second, pol_linear);
      auto &v1 = tmp.first;
      unsigned j = 0;
      unsigned n = v1.size();
      while (j < n) {
        auto &y = v1[j];
        if (any_of(v1.begin(), v1.begin() + j,
                   [y](auto z) { return (y & z) == z; }))
          y = v1[--n];
        else {
          if (any_of(v1.begin() + j + 1, v1.end(),
                     [y](auto z) { return (y & z) == z; }))
            y = v1[--n];
          else
            ++j;
        }
      }
      v1.resize(n);
      sort(v1.begin(), v1.end());
      auto &v2 = tmp.second;
      j = 0;
      n = v2.size();
      while (j < n) {
        auto &y = v2[j];
        if (any_of(v2.begin(), v2.begin() + j,
                   [y](auto z) { return (y & z) == z; }))
          y = v2[--n];
        else {
          if (any_of(v2.begin() + j + 1, v2.end(),
                     [y](auto z) { return (y & z) == z; }))
            y = v2[--n];
          else
            ++j;
        }
      }
      v2.resize(n);
      sort(v2.begin(), v2.end());
      it = table_m.emplace(x.second, move(tmp)).first;
    }
    auto const &my_pair = it->second;
    for (uint64_t y : my_pair.first) {
      uint64_t z = 0;
      for (unsigned j = 0; j < m_size; ++j)
        z |= ((y >> j) & 1) << (i + j * s_size);
      maybe.emplace_back(x.first | z);
    }
    for (uint64_t y : my_pair.second) {
      uint64_t z = 0;
      for (unsigned j = 0; j < m_size; ++j)
        z |= ((y >> j) & 1) << (i + j * s_size);
      sure.emplace_back(x.first | z);
    }
  }
//  cout << maybe.size() << " - " << sure.size() << endl;
}

void SearchMon::applyP() {
  for (auto &x : maybe) {
    uint64_t y = 0;
    for (unsigned i = 0; i < 64; ++i)
      y |= ((x >> i) & 1) << perm[i];
    x = y;
  }
  for (auto &x : sure) {
    uint64_t y = 0;
    for (unsigned i = 0; i < 64; ++i)
      y |= ((x >> i) & 1) << perm[i];
    x = y;
  }
}
