#include <mutex>
#include <execution>
#include <random>
#include <algorithm>

#include "Polynome.hpp"

using namespace std;

void searchSimon_tmp(vector<uint8_t> const & v, uint8_t b, uint32_t const i, vector<Monome> const & p, vector<uint8_t> & res) {
  {
    auto & r = res[i];
    for (auto const & m : p) {
      auto const & t = v[m];
      if (t == 2) {r = 2; break;}
      r ^= t;
    }
  }
  if (b != 0) {
    vector<Monome> p1 (p.size());
    auto beg1 = p1.begin();
    vector<Monome> p2 (p.size());
    auto beg2 = p2.begin();
    do {
      b -= 1;
      Monome const & m = 1u << b;
      auto it1 = beg1;
      auto it2 = beg2;
      for (auto const & mm : p) {
        auto const & mmm = mm | m;
        if (mmm == mm) {*it1 = mmm; ++it1;}
        else {*it2 = mmm; ++it2;}
      }
      vector<Monome> pp = merge(p1.begin(), it1, p2.begin(), it2);
      searchSimon_tmp(v, b, i | (m << 16), pp, res);
    } while (b != 0);
  }
}

void searchSimon() {
  unsigned const R = 32;
  vector<Polynome> anf_simon (32);
  for (unsigned i = 0; i < 16; ++i) anf_simon[16+i] = Monome(1u << i);
  for (unsigned i = 0; i < 16; ++i) {
    anf_simon[i] = Polynome(Monome(1u << (16+i)));
    anf_simon[i] += Polynome(Monome(1u << ((i+2)%16)));
    anf_simon[i] += Polynome(Monome(1u << ((i+1)%16)))*Polynome(Monome(1u << ((i+8)%16)));
  }
  vector<Polynome> anf_simon_linear (16);
  for (unsigned i = 0; i < 16; ++i) anf_simon_linear[i] = Polynome(Monome(1u << (16+i))) + Polynome(Monome(1u << ((i+1)%16)))*Polynome(Monome(1u << ((i+8)%16)));

  for (auto const & p : anf_simon) cout << p << endl;
  vector<Polynome> vs (1u << 16);
  vs[0] = Monome(0);
  for (unsigned k = 0; k < 16; ++k) vs[1u << k] = anf_simon[k];
  uint64_t cpt_terms = 0;
  uint64_t max_terms = 0;
  for (unsigned c = 2; c <= 9; ++c) {
    cout << "\r" << c << flush;
    #pragma omp parallel for schedule(dynamic)
    for (uint64_t i = 1; i < uint64_t(1) << 16; ++i) {
      if (__builtin_popcount(i) != c) continue;
      unsigned k = 1;
      while ((k & i) == 0) k <<= 1;
      vs[i] = vs[k]*vs[i ^ k];
      #pragma omp critical
      {
        cpt_terms += vs[i].nbTerms();
        if (vs[i].nbTerms() > max_terms) max_terms = vs[i].nbTerms();
      }
    }
  }
  cout << ": computed (" << cpt_terms << " - " << max_terms << ")" << endl;
  return;
  vector<uint32_t> vi0 (1u << 16);
  for (uint32_t i = 0; i < 1u << 16; ++i) vi0[i] = i;
  random_shuffle(vi0.begin(), vi0.end());
  uint64_t bound = uint64_t(1) << 32;
  for (unsigned b = 0; b < 32; b += 16) {
    vector<uint8_t> v (bound, 0);
    v[~uint32_t(1u << b)] = 1;
    for (unsigned r = 0; r < R; ++r) {
      vector<uint8_t> tmp (bound, 0);
      uint64_t cpt = 0;
      mutex mtx;
      for_each(execution::par, vi0.begin(), vi0.end(), [&v, &tmp, &vs, &mtx, &b, &r, &cpt](auto const & i0){
        vector<Monome> p (vs[i0].begin(), vs[i0].end());
        searchSimon_tmp(v, 16, i0, p, tmp);
        mtx.lock();
        cout << "\r" << b << ": " << r << " (" << ++cpt << ")                  " << flush;
        mtx.unlock();
      });
      // #pragma omp parallel for schedule(dynamic)
      // for (uint32_t i0 = 0; i0 < 1u << 16; ++i0) {
      //   vector<Monome> p (vs[i0].begin(), vs[i0].end());
      //   searchSimon_tmp(v, 16, i0, p, tmp);
      //   #pragma omp critical
      //   {
      //     cout << "\r" << b << ": " << r << " (" << ++cpt << ")                  " << flush;
      //   }
      // }
      bool all = true;
      for (unsigned k = 0; k < 32; ++k) all = all && (tmp[1u << k] == 2);
      if (all) {
        cout << endl << "all weight 1 vectors reached - distinguisher on " << r << " rounds: ";
        for (unsigned k = 0; k < 32; ++k) cout << k << "(" <<  (unsigned) v[1u << k] << ") ";
        cout << endl;
        for (auto const & p : anf_simon_linear) {
          auto x = 0;
          for (auto const & m : p) {
            auto const & t = v[m];
            if (t == 2) {x = 2; break;}
            x ^= t;
          }
          if (x != 2) cout << "distinguisher on " << r+1 << " rounds (linear combination) : " << p << " (" << (unsigned) x << ")" << endl;
        }
        break;
      }
      v = move(tmp);
      for (unsigned k = 0; k < 16; ++k) {
        vector<uint8_t> tmp2 (bound, 0);
        unsigned t = 1u << k;
        #pragma omp parallel for
        for (uint64_t i = 1; i < bound; ++i) {
          if ((i & t) == 0 || v[i ^ t] == 0) tmp2[i] = v[i];
          else tmp2[i] = 2;
        }
        v = move(tmp2);
      }
    }
  }
}
