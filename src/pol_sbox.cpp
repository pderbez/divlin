#include<iostream>

#include "DivTable.hpp"

using namespace std;

void printBits(uint64_t s) {
  for (unsigned b = 0; b < 64; ++b) cout << ((s >> b) & 1);
}
void printBits(uint16_t s) {
  for (unsigned b = 0; b < 16; ++b) cout << ((s >> b) & 1);
}


uint16_t getColumn(unsigned const c, uint64_t const s) {
    return (s >> 16*c) & 0xFFFF;
}

uint64_t getColumnBeforeP(unsigned const c, uint64_t const s, vector<unsigned> const & p) {
    uint16_t res = 0;
    for (unsigned i = 0; i < 16; ++i) {
        res |= ((s >> p[i+16*c]) & 1) << i;
    }
    return res;
}


void showPropagate(uint64_t in, uint64_t out, int R, vector<vector<uint16_t>> const & divtableSLKS, vector<vector<uint16_t>> const & divtableL, vector<vector<uint16_t>> const & divtableLS) {
    unsigned p[64];
    for (unsigned b = 0; b < 64; ++b) {
        unsigned l = (b/4)%4;
        p[b] = 16*((b/16 + l)%4) + b%16;
    }

    auto divtableSLKS_inv = invDivtable(divtableSLKS);
    auto divtableL_inv = invDivtable(divtableL);
    auto divtableLS_inv = invDivtable(divtableLS);

    vector<vector<uint8_t>> states (R, vector<uint8_t> (64, 2));
    for (int i = 0; i < 64; ++i) states[0][i] = (in >> i) & 1;
    for (int i = 0; i < 64; ++i) states[R-1][i] = (out >> i) & 1;
    bool forward = true;
    bool backward = true;
    while (forward || backward) {
        int r = 0;
        while (r < R-1 && forward) {
            forward = false;
            auto const & divtable = (r%2) == 0 ? divtableSLKS : ((r == R-2) ? divtableLS : divtableL);
            vector<uint8_t> tmp (64);
            for (int c = 0; c < 4; ++c) {
                uint16_t i0 = ~0;
                uint16_t i1 = ~0;
                uint16_t u0 = ~0;
                uint16_t u1 = ~0;
                for (unsigned i = 0; i < divtable.size(); ++i) {
                    unsigned j = 0;
                    while (j < 16 && (((i>>j) & 1) == states[r][j + 16*c] || states[r][j + 16*c] == 2)) ++j;
                    if (j != 16) continue;
                    bool possible = false;
                    auto const & state_r1 = states[r+1];
                    for (auto x : divtable[i]) {
                        unsigned k = 0;
                        while (k < 16 && (((x>>k) & 1) == state_r1[p[k + 16*c]] || state_r1[p[k + 16*c]] == 2)) ++k;
                        if (k != 16) continue;
                        u0 &= ~x; u1 &= x;
                        possible = true;
                    }
                    if (possible) {i0 &= ~i; i1 &= i;}
                }
                if ((u0 & u1) != 0) {
                    cout << "impossible forward: " << r << " - " << c << endl;
                    getchar();
                    return;
                }
                for (unsigned i = 0; i < 16; ++i) {
                    if (((u0 >> i) & 1) == 1) {tmp[i + 16*c] = 0; if (states[r+1][p[i + 16*c]] == 2) forward = true;}
                    else if (((u1 >> i) & 1) == 1) {tmp[i + 16*c] = 1; if (states[r+1][p[i + 16*c]] == 2) forward = true;}
                    else tmp[i + 16*c] = 2;

                    if (states[r][i + 16*c] == 2) {
                        if (((i0 >> i) & 1) == 1) {states[r][i + 16*c] = 0; backward = true;}
                        else if (((i1 >> i) & 1) == 1) {states[r][i + 16*c] = 1; backward = true;}
                    }
                }
            }
            r += 1;
            for (unsigned b = 0; b < 64; ++b) {
                states[r][p[b]] = tmp[b];
            }
        }
        forward = false;
        r = R-1;
        while (r > 0 && backward) {
            backward = false;
            auto const & divtable = (r%2) == 1 ? divtableSLKS_inv : ((r == R-1) ? divtableLS_inv : divtableL_inv);
            vector<uint8_t> tmp (64);
            for (int c = 0; c < 4; ++c) {
                uint16_t i0 = ~0;
                uint16_t i1 = ~0;
                uint16_t u0 = ~0;
                uint16_t u1 = ~0;
                for (unsigned i = 0; i < divtable.size(); ++i) {
                    unsigned j = 0;
                    while (j < 16 && (((i>>j) & 1) == states[r][p[j + 16*c]] || states[r][p[j + 16*c]] == 2)) ++j;
                    if (j != 16) continue;
                    bool possible = false;
                    auto const & state_r1 = states[r-1];
                    for (auto x : divtable[i]) {
                        unsigned k = 0;
                        while (k < 16 && (((x>>k) & 1) == state_r1[k + 16*c] || state_r1[k + 16*c] == 2)) ++k;
                        if (k != 16) continue;
                        u0 &= ~x; u1 &= x;
                        possible = true;
                    }
                    if (possible) {i0 &= ~i; i1 &= i;}
                }
                if ((u0 & u1) != 0) {
                    cout << "impossible backward: " << r << " - " << c << endl;
                    getchar();
                    return;
                }
                for (unsigned i = 0; i < 16; ++i) {
                    if (((u0 >> i) & 1) == 1) {tmp[i + 16*c] = 0; if (states[r-1][i + 16*c] == 2) backward = true;}
                    else if (((u1 >> i) & 1) == 1) {tmp[i + 16*c] = 1; if (states[r-1][i + 16*c] == 2) backward = true;}
                    else tmp[i + 16*c] = 2;

                    if (states[r][p[i + 16*c]] == 2) {
                        if (((i0 >> i) & 1) == 1) {states[r][p[i + 16*c]] = 0; forward = true;}
                        else if (((i1 >> i) & 1) == 1) {states[r][p[i + 16*c]] = 1; forward = true;}
                    }
                }
            }
            r -= 1;
            for (unsigned b = 0; b < 64; ++b) {
                states[r][b] = tmp[b];
            }
        }
        backward = false;
    }
    for (auto & v : states) {
        for (unsigned x : v) {
            if (x == 2) cout << "*";
            else cout << x;
        }
        cout << endl;
    }
}


// int main(int argc, char const *argv[]) {
//     //vector<uint16_t> sbox = {0xC, 5, 6, 0xB, 9, 0, 0xA, 0xD, 3, 0xE, 0xF, 8, 4, 7, 1, 2}; // PRESENT
//     vector<uint16_t> sbox = {0xc, 6, 9, 0, 1, 0xa, 2, 0xb, 3, 8, 5, 0xd, 4, 0xe, 7, 0xf}; // SKINNY
//     vector<vector<uint8_t>> m = {{1, 0, 1, 1}, {1,0,0,0}, {0,1,1,0}, {1,0,1,0}}; // SKINNY
//
//
//     ifstream infileL ("tableL_SKINNY64.txt");
//     DivTable divtableL (infileL);
//
//     ifstream infileLS ("tableLS_SKINNY64.txt");
//     DivTable divtableLS (infileLS);
//
//     ifstream infile ("tableSLKS_SKINNY64.txt");
//     DivTable divtableSLKS (infile);
//
//     for (unsigned i = 0; i < 16; ++i) {
//         uint64_t in = 0;
//         in = ~in;
//         in ^= 1u << i;
//         for (unsigned j = 0; j < 64; ++j) {
//             uint64_t out = uint64_t (1) << j;
//             cout << i << " - " << j << endl;
//             showPropagate(in, out, 11, divtableSLKS, divtableL, divtableLS);
//             cout << " ----------------------------- " << endl << endl;
//         }
//     }
//
//
//     return 0;
// }
