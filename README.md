# DivLin

Tool to search for distinguishers based on division property.

Compilation:
  make

Requierement:
  - a repertory named "tables" has to be created
  - libtbb-dev and libtbb2
  - lastest version of g++

Run:
  - ./divtool cipher R : search for distinguishers against R rounds of the cipher
  - ./divtool cipher R n: search for low data distinguishers against R rounds of the cipher where n SSB are set to 0. (0 <= n <= 3)


Ciphers implemented: present midori skinny hight twine gift led

---------------------------------

The repertory lowdatadist contains implementations of some low data distinguishers
  - compilation: g++ -O3 -march=native -std=c++17 -o file file.cpp -ltbb
  - run: ./file N where N is the number of experiments
