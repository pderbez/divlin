#include <execution>
#include <mutex>

#include "Search.hpp"


using namespace std;

class SearchState {
public:
  SearchState () {};
  SearchState (unsigned r) : v (r*16) {};
  SearchState (SearchState const &) = default;
  SearchState (SearchState &&) = default;

  SearchState & operator=(SearchState const & ) = default;
  SearchState & operator=(SearchState && ) = default;

  uint16_t & operator()(unsigned r, unsigned x) {return v[16*r + x];};
  uint16_t operator()(unsigned r, unsigned x) const {return v[16*r + x];};

  auto rounds() const {return v.size()/16;};


private:
  vector<uint16_t> v;
};

vector<unsigned> getCorrespondingMinimalVector(uint16_t x) {
  vector<unsigned> res (16);
  unsigned n = 0;
  for (unsigned i = 0; i < 16; ++i) {
    if (((x >> i) & 1) != 0) {
      unsigned j = 0;
      while (j < n) {
        auto const & inter = i & res[j];
        if (inter == res[j]) break;
        if (inter == i) res[j] = res[--n];
        else ++j;
      }
      if (j == n) res[n++] = i;
    }
  }
  res.resize(n);
  return res;
}

vector<unsigned> getCorrespondingVector(uint16_t x) {
  vector<unsigned> res (16);
  unsigned n = 0;
  for (unsigned i = 0; i < 16; ++i) {
    if (((x >> i) & 1) != 0) res[n++] = i;
  }
  res.resize(n);
  return res;
}

void printSearchStates(vector<vector<uint16_t>> const & searchstates) {
  for (auto const & v : searchstates) {
    for (unsigned x : v) {
      for (unsigned i = 0; i < 16; ++i) cout << ((x >> i) & 1);
      cout << " ";
    }
    cout << endl;
  }
}

void printSearchStates(SearchState const & searchstates) {
  auto const & R = searchstates.rounds();
  for (unsigned r = 0; r < R; ++r) {
    for (unsigned v = 0; v < 16; ++v) {
      auto x = searchstates(r,v);
      for (unsigned i = 0; i < 16; ++i) cout << ((x >> i) & 1);
      cout << " ";
    }
    cout << endl;
  }
}

void printSearchStatesFinal(SearchState const & searchstates) {
  auto const & R = searchstates.rounds();
  for (unsigned r = 0; r < R; ++r) {
    for (unsigned v = 0; v < 16; ++v) {
      auto x = searchstates(r,v);
      for (unsigned i = 0; i < 16; ++i) {
        if (((x >> i) & 1) != 0) {
          for (unsigned j = 0; j < 4; ++j) {
           cout << ((i >> j) & 1);
          }
        }
        //cout << ((x >> i) & 1);
      }
      cout << " ";
    }
    cout << endl;
  }
}

void printSearchStatesLateX(SearchState const & searchstates) {
  cout << "\\begin{array}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}" << endl;
  auto const & R = searchstates.rounds();
  for (unsigned v = 0; v < 16; ++v) {
    for (unsigned r = 0; r < R; ++r) {
      cout << "{";
      auto x = searchstates(r,v);
      bool first = true;
      for (unsigned i = 0; i < 16; ++i) {
        if (((x >> i) & 1) == 0) continue;
        if (!first) cout << ",";
        else first = false;
        if (i < 10) cout << i;
        else cout << (char) ('A' + i - 10);
      }
      cout << "}";
      if (r < R-1) cout << " & ";
    }
    cout << "\\\\" << endl;
  }
}

// bool propagateForward(set<unsigned> & to_try, vector<vector<uint16_t>> & searchstates, vector<vector<DivTable>> const & vdiv, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv) {
//   if (to_try.empty()) return true;
//   unsigned r = *to_try.begin();
//   to_try.erase(to_try.begin());
//   bool try_previous = false;
//   bool try_next = false;
//   vector<vector<unsigned>> v (16);
//   for (unsigned i = 0; i < 16; ++i) v[perm[i]] = getCorrespondingVector(searchstates[r][i]);
//   for (unsigned c = 0; c < 4; ++c) {
//     auto const & div = vdiv[r][c];
//     uint16_t in[4] = {0, 0, 0, 0};
//     uint16_t out[4] = {0, 0, 0, 0};
//     uint16_t tmp[4];
//     for (auto const & m0 : v[4*c]) { // check for all minimal inputs
//       for (auto const & m1 : v[4*c + 1]) {
//         uint16_t const & tmp1 = m0 | (m1 << 4);
//         for (auto const & m2 : v[4*c + 2]) {
//           uint16_t const & tmp2 = tmp1 | (m2 << 8);
//           for (auto const & m3 : v[4*c + 3]) {
//             uint16_t const & tmp3 = tmp2 | (m3 << 12);
//             auto const & l = div.transitionsFrom(tmp3);
//             bool flag_in = false;
//             for (auto const & x : l) {
//               bool flag = true;
//               for (unsigned i = 0; i < 4; ++i) {
//                 tmp[i] = 1u << ((x >> 4*i) & 0xF);
//                 flag &= ((tmp[i] & searchstates[r+1][4*c + i]) == tmp[i]); // check wether output satisfies conditions
//               }
//               if (flag) {
//                 for (unsigned i = 0; i < 4; ++i) out[i] |= tmp[i]; // save possible outputs
//                 flag_in = true; // the input is valid
//               }
//             }
//             if (flag_in) { // get valid inputs
//               in[0] |= 1u << m0;
//               in[1] |= 1u << m1;
//               in[2] |= 1u << m2;
//               in[3] |= 1u << m3;
//             }
//           }
//         }
//       }
//     }
//
//     for (unsigned i = 0; i < 4; ++i) {
//       auto & x = searchstates[r][perm_inv[4*c + i]];
//       auto const & y = x & in[i];
//       if (y != x) { // possible values at the input of SSB changed
//         if (y == 0) return false; // no solution
//         x = y;
//         try_previous = true;
//       }
//     }
//
//     for (unsigned i = 0; i < 4; ++i) {
//       auto & x = searchstates[r+1][4*c + i];
//       auto const & y = x & out[i];
//       if (y != x) { // possible values at the output of SSB changed
//         if (y == 0) return false; // no solution
//         x = y;
//         try_next = true;
//       }
//     }
//   }
//   if (try_previous && r != 0) to_try.emplace(r-1);
//   if (try_next && r < vdiv.size()-1) to_try.emplace(r+1);
//   return propagateForward(to_try, searchstates, vdiv, perm, perm_inv);
// }

bool propagateCore(unsigned r, vector<unsigned> const & allc, set<unsigned> & to_try, SearchState & searchstates, vector<vector<DivTable>> const & vdiv, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv) {
  vector<vector<unsigned>> v (4);
  for (auto c : allc)
  {
    for (unsigned i = 0; i < 4; ++i) v[i] = getCorrespondingVector(searchstates(r, perm_inv[4*c + i]));
    auto const & div = vdiv[r][c];
    uint64_t sr1 = searchstates(r+1, 4*c);
    for (unsigned i = 1; i < 4; ++i) sr1 |= uint64_t(searchstates(r+1, 4*c + i)) << 16*i;
    uint16_t in[4] = {0, 0, 0, 0};
    uint64_t out = 0;
    for (auto const & m0 : v[0]) { // check for all minimal inputs
      for (auto const & m1 : v[1]) {
        uint16_t const & tmp1 = m0 | (m1 << 4);
        for (auto const & m2 : v[2]) {
          uint16_t const & tmp2 = tmp1 | (m2 << 8);
          for (auto const & m3 : v[3]) {
            uint16_t const & tmp3 = tmp2 | (m3 << 12);
            auto const & l = div.transitionsFrom(tmp3);
            bool flag_in = false;
            for (auto const & x : l) {
              uint64_t tmp = uint64_t(1) << (x & 0xF);
              for (unsigned i = 1; i < 4; ++i) tmp |= uint64_t(1) << (((x >> 4*i) & 0xF) + 16*i);
              if ((tmp & sr1) == tmp) {
                out |= tmp;
                flag_in = true;
              }
            }
            if (flag_in) { // get valid inputs
              in[0] |= uint16_t(1) << m0;
              in[1] |= uint16_t(1) << m1;
              in[2] |= uint16_t(1) << m2;
              in[3] |= uint16_t(1) << m3;
            }
          }
        }
      }
    }

    for (unsigned i = 0; i < 4; ++i) {
      auto & x = searchstates(r, perm_inv[4*c + i]);
      uint16_t const & y = x & in[i];
      if (y != x) { // possible values at the input of SSB changed
        if (y == 0) return false; // no solution
        x = y;
        if (r != 0) to_try.emplace(((r-1) << 2) | (perm_inv[4*c + i]/4));
      }
    }

    for (unsigned i = 0; i < 4; ++i) {
      auto & x = searchstates(r+1, 4*c + i);
      uint16_t const & y = x & (out >> 16*i);
      if (y != x) { // possible values at the output of SSB changed
        if (y == 0) return false; // no solution
        x = y;
        if (r < vdiv.size()-1) to_try.emplace(((r+1) << 2) | (perm[4*c + i]/4));
      }
    }
  }
  return true;
}

bool propagateForward(set<unsigned> & to_try, SearchState & searchstates, vector<vector<DivTable>> const & vdiv, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv) {
  if (to_try.empty()) return true;
  vector<unsigned> allc;
  allc.emplace_back((*to_try.begin()) & 0x3);
  unsigned r = (*to_try.begin()) >> 2;
  to_try.erase(to_try.begin());
  while (!to_try.empty() && r == ((*to_try.begin()) >> 2)) {
    allc.emplace_back((*to_try.begin()) & 0x3);
    to_try.erase(to_try.begin());
  }
  auto res = propagateCore(r, allc, to_try, searchstates, vdiv, perm, perm_inv);

  return res && propagateForward(to_try, searchstates, vdiv, perm, perm_inv);
}

bool propagateBackward(set<unsigned> & to_try, SearchState & searchstates, vector<vector<DivTable>> const & vdiv, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv) {
  if (to_try.empty()) return true;
  vector<unsigned> allc;
  allc.emplace_back((*to_try.rbegin()) & 0x3);
  unsigned r = (*to_try.rbegin()) >> 2;
  to_try.erase(*(to_try.rbegin()));
  while (!to_try.empty() && r == ((*to_try.rbegin()) >> 2)) {
    allc.emplace_back((*to_try.rbegin()) & 0x3);
    to_try.erase(*(to_try.rbegin()));
  }
  auto res = propagateCore(r, allc, to_try, searchstates, vdiv, perm, perm_inv);
  return res && propagateBackward(to_try, searchstates, vdiv, perm, perm_inv);
}

bool searchTrail_tmp(unsigned ri, unsigned ro, SearchState & searchstates, vector<vector<DivTable>> const & vdiv, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv) {
  static mutex mt;
  static const uint16_t order[16] = {1u << 0, 1u << 1, 1u << 2, 1u << 4, 1u << 8, 1u << 3, 1u << 5, 1u << 6, 1u << 9, 1u << 10, 1u << 12, 1u << 7, 1u << 11, 1u << 13, 1u << 14, 1u << 15};
  if (ri > ro) {
    mt.lock();
    printSearchStatesFinal(searchstates);
    getchar();
    mt.unlock();
    return true; // all nibbles are set --> we found a trail!

  }

  /* choose next guess */
  int pi[2] = {17, 0};
  for (unsigned x = 0; x < 16; ++x) { // find unset nibble with smallest number of possible values
    auto const & b = __builtin_popcount(searchstates(ri, x)); // in state ri
    if (b > 1 && b < pi[0]) {pi[0] = b; pi[1] = x;}
  }
  int po[2] = {17, 0};
  for (unsigned x = 0; x < 16; ++x) { // the same in state ro
    auto const & b = __builtin_popcount(searchstates(ro,x));
    if (b > 1 && b < po[0]) {po[0] = b; po[1] = x;}
  }
  /* if all nibbles on both states ri and ro are set */
  if (pi[0] == 17 && po[0] == 17) return searchTrail_tmp(ri+1, ro-1, searchstates, vdiv, perm, perm_inv);


  vector<SearchState> v (2);
  if (pi[0] <= po[0]) {
    unsigned b = 0;
    while ((searchstates(ri, pi[1]) & order[b]) == 0) ++b;
    v[0] = searchstates;
    v[0](ri, pi[1]) = order[b];
    v[1] = move(searchstates);
    v[1](ri, pi[1]) ^= order[b];
    unsigned c0 = pi[1]/4;
    unsigned c1 = perm[pi[1]]/4;
    return any_of(execution::par, v.begin(), v.end(), [c0,c1,ri,ro,&vdiv,&perm,&perm_inv](auto & s){
      set<unsigned> to_update;
      if (ri != 0) to_update.emplace(((ri-1) << 2) | c0);
      to_update.emplace((ri << 2) | c1);
      return propagateForward(to_update, s, vdiv, perm, perm_inv) && searchTrail_tmp(ri, ro, s, vdiv, perm, perm_inv);
    });
  }
  else {
    unsigned b = 15;
    while ((searchstates(ro, po[1]) & order[b]) == 0) --b;
    v[0] = searchstates;
    v[0](ro, po[1]) = order[b];
    v[1] = move(searchstates);
    v[1](ro, po[1]) ^= order[b];
    unsigned c0 = po[1]/4;
    unsigned c1 = perm[po[1]]/4;
    return any_of(execution::par, v.begin(), v.end(), [c0,c1,ri,ro,&vdiv,&perm,&perm_inv](auto & s){
      set<unsigned> to_update;
      if (ro != 0) to_update.emplace(((ro-1) << 2) | c0);
      to_update.emplace((ro << 2) | c1);
      return propagateBackward(to_update, s, vdiv, perm, perm_inv) && searchTrail_tmp(ri, ro, s, vdiv, perm, perm_inv);
    });
  }
}

bool searchTrail_tmp2(unsigned ri, unsigned ro, SearchState & searchstates, vector<vector<DivTable>> const & vdiv, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv) {
  static const uint16_t order[5] = {1u << 0, (1u << 1) + (1u << 2) + (1u << 4) + (1u << 8), (1u << 3) + (1u << 5) + (1u << 6) + (1u << 9) + (1u << 10),
     (1u << 12) + (1u << 7) + (1u << 11) + (1u << 13) + (1u << 14), 1u << 15};
  if (ri > ro) return searchTrail_tmp(1,vdiv.size()-1, searchstates, vdiv, perm, perm_inv); // all nibbles are set --> we found a trail!


  /* choose next guess */
  int pi[2] = {17, 0};
  for (unsigned x = 0; x < 16; ++x) { // find unset nibble with smallest number of possible values
    int b = 0;
    for (unsigned i = 0; i < 5; ++i) if ((searchstates(ri, x) & order[i]) != 0) b += 1;
    if (b > 1 && b < pi[0]) {pi[0] = b; pi[1] = x;}
  }
  int po[2] = {17, 0};
  for (unsigned x = 0; x < 16; ++x) { // the same in state ro
    int b = 0;
    for (unsigned i = 0; i < 5; ++i) if ((searchstates(ro, x) & order[i]) != 0) b += 1;
    if (b > 1 && b < po[0]) {po[0] = b; po[1] = x;}
  }
  /* if all nibbles on both states ri and ro are set */
  if (pi[0] == 17 && po[0] == 17) return searchTrail_tmp2(ri+1, ro-1, searchstates, vdiv, perm, perm_inv);


  vector<SearchState> v (2);
  if (pi[0] <= po[0]) {
    unsigned b = 0;
    while ((searchstates(ri, pi[1]) & order[b]) == 0) ++b;
    v[0] = searchstates;
    v[0](ri, pi[1]) &= order[b];
    v[1] = move(searchstates);
    v[1](ri, pi[1]) &= ~order[b];
    unsigned c0 = pi[1]/4;
    unsigned c1 = perm[pi[1]]/4;
    return any_of(execution::par, v.begin(), v.end(), [c0,c1,ri,ro,&vdiv,&perm,&perm_inv](auto & s){
      set<unsigned> to_update;
      if (ri != 0) to_update.emplace(((ri-1) << 2) | c0);
      to_update.emplace((ri << 2) | c1);
      return propagateForward(to_update, s, vdiv, perm, perm_inv) && searchTrail_tmp2(ri, ro, s, vdiv, perm, perm_inv);
    });
  }
  else {
    unsigned b = 4;
    while ((searchstates(ro, po[1]) & order[b]) == 0) --b;
    v[0] = searchstates;
    v[0](ro, po[1]) &= order[b];
    v[1] = move(searchstates);
    v[1](ro, po[1]) &= ~order[b];
    unsigned c0 = po[1]/4;
    unsigned c1 = perm[po[1]]/4;
    return any_of(execution::par, v.begin(), v.end(), [c0,c1,ri,ro,&vdiv,&perm,&perm_inv](auto & s){
      set<unsigned> to_update;
      if (ro != 0) to_update.emplace(((ro-1) << 2) | c0);
      to_update.emplace((ro << 2) | c1);
      return propagateBackward(to_update, s, vdiv, perm, perm_inv) && searchTrail_tmp2(ri, ro, s, vdiv, perm, perm_inv);
    });
  }
}

bool searchTrail(uint64_t in64, uint64_t out64, vector<vector<DivTable>> vdiv, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv) {
  {
    in64 = ~uint64_t(0);
    in64 ^= uint64_t(14) << 8;
    out64 = uint64_t(0);
    out64 ^= uint64_t(14) << 8;
    cout << "in: ";
    for (unsigned i = 0; i < 64; ++i) {
      cout << ((in64 >> i) & 1);
      if (i%4 == 3) cout << " ";
    }
    cout << endl;
    cout << "out: ";
    for (unsigned i = 0; i < 64; ++i) {
      cout << ((out64 >> i) & 1);
      if (i%4 == 3) cout << " ";
    }
    cout << endl;
  }
  removeUnusedTrails(in64, out64, vdiv, perm);
  //removeUnusedTrailsIN(in64, vdiv, perm);
  //removeUnnecessaryTrails(vdiv, perm);
  SearchState searchstates (vdiv.size() + 1);
  for (unsigned r = 0; r < vdiv.size(); ++r) {
    for (unsigned i = 0; i < 16; ++i) searchstates(r, i) = 0xFFFF;
  }
  for (unsigned i = 0; i < 16; ++i) searchstates(vdiv.size(), i) = 1;
  for (unsigned r = 0; r < vdiv.size(); ++r) {
    for (unsigned c = 0; c < 4; ++c) {
      uint16_t in [4] = {0,0,0,0};
      for (unsigned i = 0; i < 1u << 16; ++i) {
        if (vdiv[r][c].transitionsFrom(i).empty()) continue;
        for (unsigned j = 0; j < 4; ++j) in[j] |= uint16_t(1) << ((i >> 4*j) & 0xF);
      }
      for (unsigned i = 0; i < 4; ++i) {
        searchstates(r, perm_inv[4*c+i]) = in[i];
      }
    }
  }

  for (unsigned i = 0; i < 16; ++i) searchstates(0, i) &= 1u << ((in64 >> 4*i) & 0xF);
  // cout << endl;
  // printSearchStates(searchstates);
  // cout << " --------------- " << endl;
  set<unsigned> to_update;
  to_update.emplace(0);
  to_update.emplace(1);
  to_update.emplace(2);
  to_update.emplace(3);
  bool res = propagateForward(to_update, searchstates, vdiv, perm, perm_inv);
  // printSearchStates(searchstates);
  // cout << " --------------- " << endl;
  return res && searchTrail_tmp2(1,vdiv.size()-1,  searchstates, vdiv, perm, perm_inv);
}


void searchDistinguishers(unsigned ci, unsigned co, vector<pair<vector<uint16_t>, unsigned>> const & s_in,
   vector<pair<vector<uint16_t>, unsigned>> const & s_out, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv,
   vector<vector<DivTable>> const & vdiv, uint64_t cibar) {

  vector<pair<unsigned, unsigned>> remain;
  for (unsigned i = 0; i < s_in.size(); ++i) {
    for (unsigned j = 0; j < s_out.size(); ++j) remain.emplace_back(i,j);
  }
  set<pair<uint16_t, uint16_t>> to_try;
  for (auto const & p : remain) {
    for (auto const & x : s_in[p.first].first) {
      for (auto const & y : s_out[p.second].first) {
        to_try.emplace(x,y);
      }
    }
  }
  //cout << remain.size() << " possible distinguishers" << endl;
  //cout << to_try.size() << " trails to search for" << endl;
  uint64_t cpt = 0;
  while (!to_try.empty()) {
    pair<uint16_t, uint16_t> tmp = *to_try.begin();
    for (auto const & p : to_try) {
      if (__builtin_popcount(p.first) <= __builtin_popcount(tmp.first) && __builtin_popcount(p.second) >= __builtin_popcount(tmp.second)) tmp = p;
    }
    to_try.erase(tmp);
    uint64_t in64 = (uint64_t(tmp.first) << 16*ci) | cibar;
    uint64_t out64 = (uint64_t(tmp.second) << 16*co);
    bool trail_found = searchTrail(in64, out64, vdiv, perm, perm_inv);
    if (trail_found) {
      unsigned i = 0;
      unsigned n = remain.size();
      while (i < n) {
        auto const & v_in = s_in[remain[i].first].first;
        if (none_of(v_in.begin(), v_in.end(), [&tmp](auto const & x){return (x & tmp.first) == x;})) ++i;
        else {
          auto const & v_out = s_out[remain[i].second].first;
          if (none_of(v_out.begin(), v_out.end(), [&tmp](auto const & x){return (x & tmp.second) == tmp.second;})) ++i;
          else remain[i] = remain[--n];
        }
      }
      remain.resize(n);
      set<pair<uint16_t, uint16_t>> to_try_tmp;
      for (auto const & p : remain) {
        for (auto const & x : s_in[p.first].first) {
          for (auto const & y : s_out[p.second].first) {
            to_try_tmp.emplace(x,y);
          }
        }
      }
      auto it = to_try.begin();
      while (it != to_try.end()) {
        if (to_try_tmp.count(*it) == 0) it = to_try.erase(it);
        else ++it;
      }
    }
    else {
      auto it = to_try.begin();
      while (it != to_try.end()) {
        if ((it->first & tmp.first) == tmp.first && (it->second & tmp.second) == it->second) it = to_try.erase(it);
        else ++it;
      }
    }
    cout << "\r" << ++cpt << " done - " << to_try.size() << " remaining         " << flush;
  }
  //return remain;
  cout << endl;
  if (!remain.empty()) cout << ci << " - " << co << ": " << remain.size() << " distinguishers found!!" << endl;
  map<unsigned, vector<unsigned>> my_map;
  for (auto const & p : remain) my_map[p.first].emplace_back(p.second);
  for (auto const & p : my_map) {
    cout << " - " << p.first << ": ";
    for (auto const & x : p.second) cout << x << " ";
    cout << endl;
  }
  //return !remain.empty();
}

void searchDistinguishers(unsigned n0sbox, unsigned ci, unsigned co, vector<pair<vector<uint16_t>, unsigned>> const & s_in,
   vector<pair<vector<uint16_t>, unsigned>> const & s_out, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv,
   vector<vector<DivTable>> const & vdiv) {

   for (unsigned i = 0; i < 1u << 4; ++i) {
     if (__builtin_popcount(i) != n0sbox+1) continue;
     if (((i >> ci) & 1) == 0) continue;
     uint64_t cibar = ~uint64_t(0);
     for (unsigned j = 0; j < 4; ++j) {
       if (((i >> j) & 1) != 0) {
         cibar ^= uint64_t(0xFFFF) << 16*j;
         cout << j << " ";
       }
     }
     cout << endl;
     searchDistinguishers(ci, co, s_in, s_out, perm, perm_inv, vdiv, cibar);
   }
}

void searchDistinguishers2(unsigned n0sbox, unsigned ci, unsigned co, vector<pair<vector<uint16_t>, unsigned>> const & s_in,
   vector<pair<vector<uint16_t>, unsigned>> const & s_out, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv,
   vector<vector<DivTable>> const & vdiv) {

   for (unsigned i = 0; i < 1u << 4; ++i) {
     if (__builtin_popcount(i) != n0sbox) continue;
     uint64_t in64 = ~uint64_t(0);
     for (unsigned j = 0; j < 4; ++j) {
       if (((i >> j) & 1) != 0) {
         in64 ^= uint64_t(0xFFFF) << 16*j;
         cout << j << " ";
       }
     }
     cout << endl;
     for (unsigned j = 0; j < s_out.size(); ++j) {
       bool flag = true;
       for (auto const & y : s_out[j].first) {
         uint64_t out64 = (uint64_t(y) << 16*co);
         flag = !searchTrail(in64, out64, vdiv, perm, perm_inv);
         if (!flag) break;
       }
       if (flag) cout << "found: " << j << endl;
     }
   }
}

void searchDistinguishers(unsigned ci, unsigned co, vector<pair<vector<uint16_t>, unsigned>> const & s_in,
   vector<pair<vector<uint16_t>, unsigned>> const & s_out, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv,
   vector<vector<DivTable>> const & vdiv) {

     return searchDistinguishers(0, ci, co, s_in, s_out, perm, perm_inv, vdiv);
}

bool searchDistinguishers3TMP(vector<uint16_t> const & v0, vector<uint16_t> const & v1, vector<uint16_t> const & v2, vector<uint16_t> const & v3,
  vector<uint16_t> const & vy, unsigned co, vector<pair<uint64_t, uint64_t>> const & trails, vector<pair<uint64_t, uint64_t>> const & notrails, vector<pair<uint64_t, uint64_t>> & res) {
  for (uint64_t x0 : v0) {
    for (uint64_t x1 : v1) {
      for (uint64_t x2 : v2) {
        for (uint64_t x3 : v3) {
          uint64_t in64 = x0 | (x1 << 16) | (x2 << 32) | (x3 << 48);
          for (uint64_t y0 : vy) {
            uint64_t out64 = y0 << 16*co;
            pair<uint64_t, uint64_t> p (in64, out64);
            if (any_of(trails.begin(), trails.end(), [&p](auto const & pp){
              return (pp.first & p.first) == p.first && (pp.second & p.second) == pp.second;
            })) return true;
            else {
              if (any_of(notrails.begin(), notrails.end(), [&p](auto const & pp){
                return (pp.first & p.first) == pp.first && (pp.second & p.second) == p.second;
              })) continue;
              else res.emplace_back(p);
            }
          }
        }
      }
    }
  }
  sort(res.begin(), res.end(), [](auto const & x1, auto const & x2){
    auto const & n1 = __builtin_popcountll(x1.first) - __builtin_popcountll(x1.second);
    auto const & n2 = __builtin_popcountll(x2.first) - __builtin_popcountll(x2.second);
    return n1 > n2 || (n1 == n2 && x1 < x2);
  });
  return false;
}

void searchDistinguishers3(int n0sbox, unsigned co, vector<vector<pair<vector<uint16_t>, unsigned>>> const & s_in,
   vector<pair<vector<uint16_t>, unsigned>> const & s_out, vector<uint8_t> const & perm, vector<uint8_t> const & perm_inv,
   vector<vector<DivTable>> const & vdiv) {

     vector<pair<uint64_t, uint64_t>> trails;
     vector<pair<uint64_t, uint64_t>> notrails;

     auto s_in2 = s_in;
     for (unsigned i = 0; i < 4; ++i) {
       s_in2[i].emplace_back(std::vector<uint16_t> (1, 0xFFFF), 0xFFFF1);
       s_in2[i].emplace_back(std::vector<uint16_t> (1, 0), 0xFFFF0);
     }

     auto s_out2 = s_out;

     for (auto const & vy0 : s_out2) {
       for (int nA = 3; nA >= 0; --nA) {
         for (auto const & vx0 : s_in2[0]) {
           for (auto const & vx1 : s_in2[1]) {
             for (auto const & vx2 : s_in2[2]) {
               for (auto const & vx3 : s_in2[3]) {
                 int cpt = 0;
                 if (vx0.second == 0xFFFF1) cpt += 1;
                 if (vx1.second == 0xFFFF1) cpt += 1;
                 if (vx2.second == 0xFFFF1) cpt += 1;
                 if (vx3.second == 0xFFFF1) cpt += 1;
                 if (cpt != nA) continue;
                 cpt = 0;
                 if (vx0.second == 0xFFFF0) cpt += 1;
                 if (vx1.second == 0xFFFF0) cpt += 1;
                 if (vx2.second == 0xFFFF0) cpt += 1;
                 if (vx3.second == 0xFFFF0) cpt += 1;
                 if (cpt != n0sbox) continue;
                 vector<pair<uint64_t, uint64_t>> totry;
                 bool flag = searchDistinguishers3TMP(vx0.first, vx1.first, vx2.first, vx3.first, vy0.first, co, trails, notrails, totry);
                 while (!flag && !totry.empty()) {
                   auto const & p = totry.back();
                   flag = searchTrail(p.first, p.second, vdiv, perm, perm_inv);
                   if (flag) {
                     unsigned j = 0;
                     unsigned ntrails = trails.size();
                     while (j != ntrails) {
                       auto & pp = trails[j];
                       if ((pp.first & p.first) == pp.first && (pp.second & p.second) == p.second) pp = trails[--ntrails];
                       else ++j;
                     }
                     trails.resize(ntrails);
                     trails.emplace_back(p);
                   }
                   else {
                     unsigned j = 0;
                     unsigned ntrails = notrails.size();
                     while (j != ntrails) {
                       auto & pp = notrails[j];
                       if ((pp.first & p.first) == p.first && (pp.second & p.second) == pp.second) pp = notrails[--ntrails];
                       else ++j;
                     }
                     notrails.resize(ntrails);
                     notrails.emplace_back(p);
                   }
                   totry.pop_back();
                 }
                 if (!flag) {
                   if (vx0.second == 0xFFFF0) cout << "0 ";
                   else if (vx0.second == 0xFFFF1) cout << "A ";
                   else cout << vx0.second << " ";
                     if (vx1.second == 0xFFFF0) cout << "0 ";
                   else if (vx1.second == 0xFFFF1) cout << "A ";
                   else cout << vx1.second << " ";
                     if (vx2.second == 0xFFFF0) cout << "0 ";
                   else if (vx2.second == 0xFFFF1) cout << "A ";
                   else cout << vx2.second << " ";
                     if (vx3.second == 0xFFFF0) cout << "0 ";
                   else if (vx3.second == 0xFFFF1) cout << "A ";
                   else cout << vx3.second << " ";
                   cout << " --> ";
                   for (unsigned j = 0; j < co; ++j) cout << "0 ";
                   cout << vy0.second << " ";
                   for (unsigned j = co+1; j < 4; ++j) cout << "0 ";
                   cout << endl;
                 }
               }
             }
           }
         }
       }
     }
}
