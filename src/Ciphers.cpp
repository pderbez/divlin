#include "DivTable.hpp"
#include "Polynome.hpp"
#include "Ciphers.hpp"

using namespace std;

vector<pair<uint32_t, uint32_t>> forbidDiv(DivTable const & div, unsigned n) {
  vector<vector<uint32_t>> v (1u << n);
  #pragma omp parallel for schedule(dynamic)
  for (uint32_t x = 0; x < 1u << n; ++x) {
    auto const & dx = div.transitionsFrom(x);
    auto & vx = v[x];
    for (uint32_t y = 1u << n; y-- != 0; ) {
      if (none_of(dx.begin(), dx.end(), [y](auto z){return (y & z) == z;})){ // x --> y is impossible
        if (none_of(vx.begin(), vx.end(), [y](auto z){return (y & z) == y;})) vx.emplace_back(y); // y is maximal
      }
    }
  }

  map<uint32_t, vector<uint32_t>> my_map;
  for (uint32_t x = 0; x < 1u << n; ++x) {
    auto const & vx = v[x];
    for (auto y : vx) {
      auto & my = my_map[y]; // x --> y is impossible and y is maximal
      if (none_of(my.begin(), my.end(), [x](auto z){return (x & z) == z;})) my.emplace_back(x); // x is minimal
    }
  }

  uint32_t const & mask = (1u << n) - 1;

  vector<pair<uint32_t, uint32_t>> res;
  for (auto const & p : my_map) {
    for (uint32_t x : p.second) { // x --> p.first is impossible
      res.emplace_back((mask & ~x) | ((mask & p.first) << n), x);
    }
  }
  return res;
}

uint64_t hashANF(vector<Polynome> const & anf) {
  uint64_t seed = anf.size();
  for (auto const & p : anf) {
    seed ^= p.nbTerms() + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    for (auto const & x : p) seed ^= x + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  }
  return seed;
}

DivTable loadDivTable(vector<Polynome> const & anf) {
  auto const & h = hashANF(anf);
  string name = "tables/table_" + to_string(h) + ".txt";
  ifstream infile(name);
  if (infile.good()) return DivTable (infile);
  cout << "table does not exist, computing it (may take a long time)" << endl;
  DivTable div (anf);
  cout << "\r" << " - computed" << endl;
  ofstream outfile(name);
  div.save(outfile);
  return div;
}

vector<pair<vector<uint16_t>, unsigned>> loadTransitionsIN(vector<Polynome> const & anf) {
  auto const & h = hashANF(anf);
  string name = "tables/tableIN_" + to_string(h) + ".txt";
  ifstream infile(name);
  if (infile.good()) return loadTransitions(infile);
  cout << "table does not exist, computing it (may take a long time)" << endl;
  auto s_in = getTransitionsIn(anf);
  cout << "\r" << " - computed" << endl;
  ofstream outfile(name);
  saveTransitions(outfile, s_in);
  return s_in;
}

vector<pair<vector<uint16_t>, unsigned>> loadTransitionsOUT(vector<Polynome> const & anf) {
  auto const & h = hashANF(anf);
  string name = "tables/tableOUT_" + to_string(h) + ".txt";
  ifstream infile(name);
  if (infile.good()) return loadTransitions(infile);
  auto s_out = getTransitionsOut(anf);
  ofstream outfile(name);
  saveTransitions(outfile, s_out);
  return s_out;
}

void printTransitions(vector<pair<vector<uint16_t>, unsigned>> const & s) {
  for (auto const & my_pair : s) {
    cout << " - " << (unsigned) my_pair.second << ": ";
    for (auto const & m : my_pair.first) {printAsMonome(m); cout << " ";}
    cout << endl;
  }
}

vector<Polynome> modularAddition(vector<Polynome> const & x, vector<Polynome> const & y) {
  vector<Polynome> z (x.size());
  vector<Polynome> c (x.size());
  z[0] = x[0] + y[0];
  c[0] = x[0]*y[0];
  for (unsigned i = 1; i < x.size(); ++i) {
    z[i] = x[i] + y[i] + c[i-1];
    c[i] = (x[i] + c[i-1])*(y[i] + c[i-1]) + c[i-1];
  }
  return z;
}

vector<Polynome> F0(vector<Polynome> const & x) {
  unsigned const n = x.size();
  vector<Polynome> y (n);
  for (unsigned i = 0; i < n; ++i) {
    y[i] = x[(i+n-1)%n] + x[(i+n-2)%n] + x[(i+n-7)%n];
  }
  return y;
}

vector<Polynome> F1(vector<Polynome> const & x) {
  unsigned const n = x.size();
  vector<Polynome> y (n);
  for (unsigned i = 0; i < n; ++i) {
    y[i] = x[(i+n-3)%n] + x[(i+n-4)%n] + x[(i+n-6)%n];
  }
  return y;
}

SearchParam Hight(unsigned R) {
  vector<Polynome> x0 (8);
  for (unsigned i = 0; i < 8; ++i) x0[i] = Monome(1u << i);
  vector<Polynome> x1 (8);
  for (unsigned i = 0; i < 8; ++i) x1[i] = Monome(1u << (i+8));
  vector<Polynome> k (8);
  for (unsigned i = 0; i < 8; ++i) k[i] = Monome(1u << (i+16));

  auto f1 = F1(x0);
  for (unsigned i = 0; i < 8; ++i) f1[i] += k[i];
  auto y1 = modularAddition(f1, x1);
  vector<Polynome> anf1 (16);
  for (unsigned i = 0; i < 8; ++i) anf1[i] = x0[i];
  for (unsigned i = 0; i < 8; ++i) anf1[i+8] = y1[i];

  auto f0 = F0(x0);
  auto y0 = modularAddition(k, f0);
  for (unsigned i = 0; i < 8; ++i) y0[i] += x1[i];
  vector<Polynome> anf0 (16);
  for (unsigned i = 0; i < 8; ++i) anf0[i] = x0[i];
  for (unsigned i = 0; i < 8; ++i) anf0[i+8] = y0[i];

  auto const & d0 = loadDivTable (anf0);
  auto const & d1 = loadDivTable (anf1);

  vector<DivTable> v = {d1, d0, d1, d0};

  vector<uint8_t> perm = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1};
  vector<uint8_t> perm_inv (16);
  for (unsigned i = 0; i < 16; ++i) perm_inv[perm[i]] = i;

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_in (4);
  vs_in[0] = loadTransitionsIN(anf1);
  vs_in[2] = vs_in[0];
  cout << "Transitions IN (columns 0 and 2): " << endl;
  printTransitions(vs_in[0]);
  cout << " -------------------------- " << endl;

  vs_in[1] = loadTransitionsIN(anf0);
  vs_in[3] = vs_in[1];
  cout << "Transitions IN (columns 1 and 3): " << endl;
  printTransitions(vs_in[1]);
  cout << " -------------------------- " << endl;

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_out (4);
  vs_out[0] = loadTransitionsOUT(anf1);
  vs_out[2] = vs_out[0];
  cout << "Transitions OUT (columns 0 and 2): " << endl;
  printTransitions(vs_out[0]);
  cout << " -------------------------- " << endl;

  vs_out[1] = loadTransitionsOUT(anf0);
  vs_out[3] = vs_out[1];
  cout << "Transitions OUT (columns 1 and 3): " << endl;
  printTransitions(vs_out[1]);
  cout << " -------------------------- " << endl;

  vector<vector<DivTable>> vdiv (R-2, v);

  return SearchParam(move(vdiv), move(vs_in), move(vs_out), move(perm), move(perm_inv));
}

// void saveDivTableToFile(DivTable const & d,
// 						string const & filename){
// 	//Save a division table #T to the file #filename
//
// 	auto file = fstream(filename, ios::out | ios::binary);
//
//   vector<vector<uint32_t>> T (1u << 16);
//   for (unsigned i = 0; i < 1u << 16; ++i) {
//     T[i] = vector<uint32_t> (d.transitionsFrom(i).begin(), d.transitionsFrom(i).end());
//   }
// 	//First 32-bit chunk, size of the table
// 	uint32_t Tsize = T.size();
// 	file.write((char*)&Tsize, 4);
// 	for(auto const & v : T){
// 		//Size of v
// 		uint32_t vsize = v.size();
// 		file.write((char*)&vsize, 4);
// 		//Content of v, exploit that content of a vector is contiguous
// 		file.write((char*)&v[0], 4*v.size());
// 	}
// 	file.close();
// }


SearchParam Led(unsigned R) {
  vector<uint16_t> sbox = {12, 5, 6, 11, 9, 0, 10, 13, 3, 14, 15, 8, 4, 7, 1, 2};
  vector<uint16_t> linear = {
    0b1000100000010100,
    0b1001100100101100,
    0b0010001001001001,
    0b0100010010000010,
    0b1100010111000010,
    0b0101111001010110,
    0b1011110110111100,
    0b0110101001101001,
    0b0011101011101011,
    0b0100111100111101,
    0b1000111001111010,
    0b0001110111110101,
    0b1011111110001000,
    0b1101000110011001,
    0b1010001100100010,
    0b0101011101000100
  };

  vector<uint8_t> perm = {0, 13, 10, 7, 4, 1, 14, 11, 8, 5, 2, 15, 12, 9, 6, 3};
  vector<uint8_t> perm_inv (16);
  for (unsigned i = 0; i < 16; ++i) perm_inv[perm[i]] = i;

  auto anf_s = getANF_Parallel_S(4, sbox);
  auto anf_l = convertLtoPolynome(linear);

  auto anf_los = composition(anf_l, anf_s);
  auto anf_solos = composition(anf_s, anf_los);

  vector<vector<Polynome>> anf (4);
  anf[2] = anf_solos;
  anf[3] = anf_solos;

  uint8_t cst0 [4] = {(64>>4)&0xf, ((64>>4)&0xf)^1, (64 & 0xf)^2, (64 & 0xf)^3};
  anf[0] = anf_los;
  for (unsigned i = 0; i < 4; ++i) {
    for (unsigned j = 0; j < 4; ++j) if (((cst0[i] >> j) & 1) != 0) anf[0][4*i + j] += Monome(0);
  }
  anf[0] = composition(anf_s, anf[0]);
  anf[1] = anf_los;
  for (unsigned i = 0; i < 2; ++i) {
    for (unsigned j = 0; j < 3; ++j) {
      anf[1][4*i + j] += Monome(1u << (16 + (3*i + j)));
      anf[1][4*(i+2) + j] += Monome(1u << (16 + (3*i + j)));
    }
  }
  anf[1] = composition(anf_s, anf[1]);

  auto anf_sol = composition(anf_s, anf_l);

  auto const & dL = loadDivTable (anf_l);
  vector<DivTable> vSLKS;
  for (unsigned i = 0; i < 4; ++i) vSLKS.emplace_back(loadDivTable(anf[i]));

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_in (4);
  for (unsigned i = 0; i < 4; ++i) {
    vs_in[i] = loadTransitionsIN(anf[i]);
    cout << "Transitions IN (column " << i << ": " << endl;
    printTransitions(vs_in[i]);
    cout << " -------------------------- " << endl;
  }

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_out (4);
  if (R%2 == 0) {
    for (unsigned i = 0; i < 4; ++i) {
      vs_out[i] = loadTransitionsOUT(anf[i]);
      cout << "Transitions OUT (column " << i << ": " << endl;
      printTransitions(vs_out[i]);
      cout << " -------------------------- " << endl;
    }
  }
  else {
    for (unsigned i = 0; i < 4; ++i) vs_out[i] = loadTransitionsOUT(anf_sol);
    cout << "Transitions OUT (columns 0-3): " << endl;
    printTransitions(vs_out[0]);
    cout << " -------------------------- " << endl;
  }

  vector<DivTable> vL (4, dL);
  vector<vector<DivTable>> vdiv;
  R -= 2;
  while (R >= 3) {
    vdiv.emplace_back(vL);
    vdiv.emplace_back(vSLKS);
    R -= 2;
  }
  if (R == 2) vdiv.emplace_back(vL);

  return SearchParam(move(vdiv), move(vs_in), move(vs_out), move(perm), move(perm_inv));


}

SearchParam Twine (unsigned R) {
  vector<uint16_t> sbox = {0xC,0,0xF,0xA,2,0xB,9,5,8,3,0xD,7,1,0xE,6,4};

  auto const & anf_s = getANF_S(sbox);
  vector<vector<Polynome>> anf (4);
  for (unsigned i = 0; i < 4; ++i) {
    for (unsigned j = 0; j < 4; ++j) anf[i].emplace_back(Monome(1u << (4*i + j)));
  }
  unsigned k = 0;
  for (unsigned i = 0; i < 4; i += 2) {
    vector<Polynome> tmp = anf[i];
    for (unsigned j = 0; j < 4; ++j) {tmp[j] += Monome(1u << (16 + k)); ++k;}
    tmp = composition(anf_s, tmp);
    for (unsigned j = 0; j < 4; ++j) anf[i+1][j] += tmp[j];
  }
  vector<Polynome> anf_1r;
  for (auto const & vp : anf) for (auto const & p : vp) anf_1r.emplace_back(p);

  for (unsigned i = 0; i < 4; i += 2) {
    vector<Polynome> tmp = anf[i+1];
    for (unsigned j = 0; j < 4; ++j) {tmp[j] += Monome(1u << (16 + k)); ++k;}
    tmp = composition(anf_s, tmp);
    for (unsigned j = 0; j < 4; ++j) anf[(i+2)%4][j] += tmp[j];
  }
  swap(anf[1], anf[3]);
  vector<Polynome> anf_2r;
  for (auto const & vp : anf) for (auto const & p : vp) anf_2r.emplace_back(p);

  vector<uint8_t> perm = {12, 9, 0, 5, 6, 3, 10, 15, 4, 1, 8, 13, 14, 11, 2, 7};
  vector<uint8_t> perm_inv (16);
  for (unsigned i = 0; i < 16; ++i) perm_inv[perm[i]] = i;

  vector<DivTable> vSS (4, loadDivTable(anf_2r));
  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_in (4, loadTransitionsIN(anf_2r));
  cout << "Transitions IN (columns 0-3): " << endl;
  printTransitions(vs_in[0]);
  cout << " -------------------------- " << endl;

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_out (4, (R%2 == 0) ? loadTransitionsOUT(anf_2r) : loadTransitionsOUT(anf_1r));
  cout << "Transitions OUT (columns 0-3): " << endl;
  printTransitions(vs_out[0]);
  cout << " -------------------------- " << endl;

  vector<vector<DivTable>> vdiv;
  R -= 2;
  while (R >= 3) {
    vdiv.emplace_back(vSS);
    R -= 2;
  }

  return SearchParam(move(vdiv), move(vs_in), move(vs_out), move(perm), move(perm_inv));

}

SearchParam Gift (unsigned R) {
  vector<uint16_t> sbox = {1, 0xa, 4, 0xc, 6, 0xf, 3, 9, 2, 0xd, 0xb, 7, 5, 0, 8, 0xe};

  // 0  17 34 51 48  1  18 35 32 49  2  19 16 33 50  3

  vector<uint16_t> linear = {
    0b0000'0000'0000'0001,
    0b0000'0000'0010'0000,
    0b0000'0100'0000'0000,
    0b1000'0000'0000'0000,
    0b0001'0000'0000'0000,
    0b0000'0000'0000'0010,
    0b0000'0000'0100'0000,
    0b0000'1000'0000'0000,
    0b0000'0001'0000'0000,
    0b0010'0000'0000'0000,
    0b0000'0000'0000'0100,
    0b0000'0000'1000'0000,
    0b0000'0000'0001'0000,
    0b0000'0010'0000'0000,
    0b0100'0000'0000'0000,
    0b0000'0000'0000'1000
  };

  vector<uint8_t> perm = {0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15};
  vector<uint8_t> perm_inv (16);
  for (unsigned i = 0; i < 16; ++i) perm_inv[perm[i]] = i;

  // uint8_t cst [48] = {0x01,0x03,0x07,0x0F,0x1F,0x3E,0x3D,0x3B,0x37,0x2F,0x1E,0x3C,
  //   0x39,0x33,0x27,0x0E,0x1D,0x3A,0x35,0x2B,0x16,0x2C,0x18,0x30,0x21,0x02,0x05,
  //   0x0B,0x17,0x2E,0x1C,0x38,0x31,0x23,0x06,0x0D,0x1B,0x36,0x2D,0x1A,0x34,0x29,
  //   0x12,0x24,0x08,0x11,0x22,0x04};

  auto anf_s = getANF_Parallel_S(4, sbox);
  auto anf_l = convertLtoPolynome(linear);

  auto anf_sol = composition(anf_s, anf_l);

  vector<vector<Polynome>> v_anf (4, anf_s);
  //3, 7, 11, 15, 19, 23 --> 15, 31, 47, 63, 11, 27
  //63 --> 51
  v_anf[0][11] += Monome(uint32_t(1) << 16); // 11
  v_anf[0][15] += Monome(uint32_t(1) << 17); // 15
  v_anf[1][11] += Monome(uint32_t(1) << 16); // 27
  v_anf[1][15] += Monome(uint32_t(1) << 17); // 31
  v_anf[2][15] += Monome(uint32_t(1) << 16); // 47
  v_anf[3][3] += Monome(0); // 51
  v_anf[3][15] += Monome(uint32_t(1) << 16); // 63

  for (auto & vp : v_anf) vp = composition(anf_l, vp);

  for (auto & vp : v_anf) {
    for (unsigned k = 0; k < 4; ++k) {
      vp[4*k] += Monome(uint32_t(1) << (18 + k));
      vp[4*k+1] += Monome(uint32_t(1) << (18 + k + 4));
    }
  }

  for (auto & vp : v_anf) vp = composition(anf_s, vp);

  vector<DivTable> vSLKS;
  for (auto const & vp : v_anf) vSLKS.emplace_back(loadDivTable(vp));
  vector<DivTable> vL (4, loadDivTable(anf_l));

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_in;
  for (auto const & vp : v_anf) vs_in.emplace_back(loadTransitionsIN(vp));
  for (unsigned i = 0; i < 4; ++i) {
    cout << "Transitions IN (column " << i << "): " << endl;
    printTransitions(vs_in[i]);
    cout << " -------------------------- " << endl;
  }

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_out;
  if (R%2 == 0) {
    for (auto const & vp : v_anf) vs_out.emplace_back(loadTransitionsOUT(vp));
    for (unsigned i = 0; i < 4; ++i) {
      cout << "Transitions OUT (column " << i << "): " << endl;
      printTransitions(vs_out[i]);
      cout << " -------------------------- " << endl;
    }
  }
  else {
    vs_out = vector<vector<pair<vector<uint16_t>, unsigned>>> (4, loadTransitionsOUT(anf_sol));
    cout << "Transitions OUT (columns 0-3): " << endl;
    printTransitions(vs_out[0]);
    cout << " -------------------------- " << endl;
  }

  vector<vector<DivTable>> vdiv;
  R -= 2;
  while (R >= 3) {
    vdiv.emplace_back(vL);
    vdiv.emplace_back(vSLKS);
    R -= 2;
  }
  if (R == 2) vdiv.emplace_back(vL);

  return SearchParam(move(vdiv), move(vs_in), move(vs_out), move(perm), move(perm_inv));
}

SearchParam Present(unsigned R) {
  vector<uint16_t> sbox = {0xC, 5, 6, 0xB, 9, 0, 0xA, 0xD, 3, 0xE, 0xF, 8, 4, 7, 1, 2}; // PRESENT
  // vector<unsigned> perm(64) = {  // PRESENT
  //   0, 16, 32, 48, 1, 17, 33, 49, 2, 18, 34, 50, 3, 19, 35, 51,
  //   4, 20, 36, 52, 5, 21, 37, 53, 6, 22, 38, 54, 7, 23, 39, 55,
  //   8, 24, 40, 56, 9, 25, 41, 57, 10, 26, 42, 58, 11, 27, 43, 59,
  //   12, 28, 44, 60, 13, 29, 45, 61, 14, 30, 46, 62, 15, 31, 47, 63
  // };

  vector<uint16_t> linear = {
    0b0000'0000'0000'0001,
    0b0000'0000'0001'0000,
    0b0000'0001'0000'0000,
    0b0001'0000'0000'0000,
    0b0000'0000'0000'0010,
    0b0000'0000'0010'0000,
    0b0000'0010'0000'0000,
    0b0010'0000'0000'0000,
    0b0000'0000'0000'0100,
    0b0000'0000'0100'0000,
    0b0000'0100'0000'0000,
    0b0100'0000'0000'0000,
    0b0000'0000'0000'1000,
    0b0000'0000'1000'0000,
    0b0000'1000'0000'0000,
    0b1000'0000'0000'0000
  };

  vector<uint8_t> perm = {0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15};
  vector<uint8_t> perm_inv (16);
  for (unsigned i = 0; i < 16; ++i) perm_inv[perm[i]] = i;

  auto anf_s = getANF_Parallel_S(4, sbox);
  auto anf_l = convertLtoPolynome(linear);

  auto anf_los = composition(anf_l, anf_s);
  auto anf_kolos = anf_los;
  for (unsigned k = 0; k < 16; ++k) anf_kolos[k] += Monome(uint32_t(1) << (16 + k));
  auto anf_sokolos = composition(anf_s, anf_kolos);

  auto anf_sol = composition(anf_s, anf_l);

  auto const & dL = loadDivTable (anf_l);
  auto const & dSLKS = loadDivTable (anf_sokolos);

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_in (4, loadTransitionsIN(anf_sokolos));
  cout << "Transitions IN (columns 0-3): " << endl;
  printTransitions(vs_in[0]);
  cout << " -------------------------- " << endl;

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_out (4, R%2 == 0 ? loadTransitionsOUT(anf_sokolos) : loadTransitionsOUT(anf_sol));
  cout << "Transitions OUT (columns 0-3): " << endl;
  printTransitions(vs_out[0]);
  cout << " -------------------------- " << endl;

  vector<DivTable> vSLKS (4, dSLKS);
  vector<DivTable> vL (4, dL);
  vector<vector<DivTable>> vdiv;
  R -= 2;
  while (R >= 3) {
    vdiv.emplace_back(vL);
    vdiv.emplace_back(vSLKS);
    R -= 2;
  }
  if (R == 2) vdiv.emplace_back(vL);

  return SearchParam(move(vdiv), move(vs_in), move(vs_out), move(perm), move(perm_inv));


}

vector<vector<pair<vector<uint8_t>, vector<uint8_t>>>> allInOutPairsAES() {
vector<uint16_t> sbox = {
//0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 };

auto anf_s = getANF_S(sbox);

auto in = loadTransitionsIN(anf_s);
auto out = loadTransitionsOUT(anf_s);

cout << "in: " << in.size() << endl;
cout << "out: " << out.size() << endl;

vector<vector<pair<vector<uint8_t>, vector<uint8_t>>>> res;
for (unsigned i = 0; i < 16; ++i) { // position sbox in
for (unsigned j = 0; j < 4; ++j) {
  for (auto pin : in) {
    for (auto pout : out) {
      cout << "pin: " << pin.first.size() << endl;
      cout << "pout: " << pout.first.size() << endl;
      vector<pair<vector<uint8_t>, vector<uint8_t>>> tmp;
      for (auto xin : pin.first) {
        for (auto xout : pout.first) {
          vector<uint8_t> in_8t (128, 1);
          vector<uint8_t> out_8t (128, 1);
          for (unsigned b = 0; b < 8; ++b) in_8t[8*i + b] = (xin >> b) & 1;
          for (unsigned b = 0; b < 8; ++b) out_8t[8*j + b] = (xout >> b) & 1;
          tmp.emplace_back(move(in_8t), move(out_8t));
        }
      }
      res.emplace_back(move(tmp));
    }
  }
}
}

return res;

}

SearchParam Skinny(unsigned R) {

  {
    vector<Polynome> anf (5);
    anf[0] += Monome(uint32_t(1) << 0);
    anf[0] += Polynome(Monome(uint32_t(1) << 1))*Monome(uint32_t(1) << 2);
    anf[1] += Monome(uint32_t(1) << 1);
    anf[2] += Monome(uint32_t(1) << 2);
    anf[3] += Monome(uint32_t(1) << 3);
    anf[3] += Polynome(Monome(uint32_t(1) << 1))*Monome(uint32_t(1) << 2);
    anf[4] += Monome(uint32_t(1) << 4);

    set<uint16_t> s;
    for (unsigned i = 0; i < 1u << 10; ++i) s.emplace(i);

    for (unsigned l = 0; l < 1u << 5; ++l) {
      Polynome p (0);
      for (unsigned i = 0; i < 5; ++i) {
        if (((l >> i) & 1) != 0) {
          p *= anf[i];
          cout << 1;
        }
        else cout << 0;
      }
      cout << ": " << p << endl;
      for (uint16_t m : p) {
        m |= l << 5;
        s.erase(m);
      }
    }
    for (auto const & u : s) {
      cout << "(";
      for (unsigned i = 0; i < 10; ++i) {
        if (((u >> i) & 1) == 1) cout << "!";
        cout << (char) ('a' + i);
        if (i < 9) cout << "&";
      }
      cout << ")|";
    }
    getchar();

    auto const & dL = loadDivTable (anf);

    auto f = forbidDiv(dL, 5);
    for (auto const & v : f) {
      for (unsigned i = 0; i < 10; ++i) cout << ((v.first >> i) & 1) << " ";
      cout << " | ";
      for (unsigned i = 0; i < 10; ++i) cout << ((v.second >> i) & 1) << " ";
      cout << endl;
    }
    getchar();
  }

  vector<uint16_t> sbox = {0xc, 6, 9, 0,   1, 0xa, 2, 0xb,
                            3,   8, 5, 0xd, 4, 0xe, 7, 0xf}; // SKINNY

  vector<uint16_t> linear = {
    0b0001'0001'0000'0001,
    0b0010'0010'0000'0010,
    0b0100'0100'0000'0100,
    0b1000'1000'0000'1000,
    0b0000'0000'0000'0001,
    0b0000'0000'0000'0010,
    0b0000'0000'0000'0100,
    0b0000'0000'0000'1000,
    0b0000'0001'0001'0000,
    0b0000'0010'0010'0000,
    0b0000'0100'0100'0000,
    0b0000'1000'1000'0000,
    0b0000'0001'0000'0001,
    0b0000'0010'0000'0010,
    0b0000'0100'0000'0100,
    0b0000'1000'0000'1000
  };

  vector<uint8_t> perm_inv = {0, 13, 10, 7, 4, 1, 14, 11, 8, 5, 2, 15, 12, 9, 6, 3};
  vector<uint8_t> perm (16);
  for (unsigned i = 0; i < 16; ++i) perm[perm_inv[i]] = i;

  auto anf_s = getANF_Parallel_S(4, sbox);

  auto anf_kos = anf_s;
  for (unsigned k = 0; k < 8; ++k) {
    anf_kos[k] += Monome(uint32_t(1) << (16 + k));
    //anf_kos[4*k+1] += Monome(uint32_t(1) << (16 + k + 4));
  }
  auto anf_l = convertLtoPolynome(linear);
  auto anf_lokos = composition(anf_l, anf_kos);
  auto anf_solokos = composition(anf_s, anf_lokos);

  auto anf_sol = composition(anf_s, anf_l);

  auto anf_cokos = anf_kos;
  anf_cokos[9] += Monome(0); //add 1
  auto anf_locokos = composition(anf_l, anf_cokos);
  auto anf_solocokos = composition(anf_s, anf_locokos);

  auto const & dL = loadDivTable (anf_l);
  auto const & dSKLS = loadDivTable (anf_solokos);
  auto const & dSKCLS = loadDivTable (anf_solocokos);

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_in;
  {
    auto const & s_in_SKLS = loadTransitionsIN(anf_solokos);
    auto const & s_in_SKCLS = loadTransitionsIN(anf_solocokos);

    cout << "Transitions IN (column 0): " << endl;
    printTransitions(s_in_SKCLS);
    cout << " -------------------------- " << endl;
    cout << "Transitions IN (column 1-3): " << endl;
    printTransitions(s_in_SKLS);
    cout << " -------------------------- " << endl;

    vs_in.emplace_back(s_in_SKCLS);
    for (unsigned i = 0; i < 3; ++i) vs_in.emplace_back(s_in_SKLS);
  }

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_out;
  if (R%2 == 0) {
    auto const & s_out_SKLS = loadTransitionsOUT(anf_solokos);
    auto const & s_out_SKCLS = loadTransitionsOUT(anf_solocokos);

    cout << "Transitions OUT (column 0): " << endl;
    printTransitions(s_out_SKCLS);
    cout << " -------------------------- " << endl;
    cout << "Transitions OUT (column 1-3): " << endl;
    printTransitions(s_out_SKLS);
    cout << " -------------------------- " << endl;

    vs_out.emplace_back(s_out_SKCLS);
    for (unsigned i = 0; i < 3; ++i) vs_out.emplace_back(s_out_SKLS);
  }
  else {
    vs_out = vector<vector<pair<vector<uint16_t>, unsigned>>> (4, loadTransitionsOUT(anf_sol));

    cout << "Transitions OUT (columns 0-3): " << endl;
    printTransitions(vs_out[0]);
    cout << " -------------------------- " << endl;
  }

  vector<DivTable> vSLKS = {dSKCLS, dSKLS, dSKLS, dSKLS};
  vector<DivTable> vL (4, dL);
  vector<vector<DivTable>> vdiv;
  R -= 2;
  while (R >= 3) {
    vdiv.emplace_back(vL);
    vdiv.emplace_back(vSLKS);
    R -= 2;
  }
  if (R == 2) vdiv.emplace_back(vL);

  return SearchParam(move(vdiv), move(vs_in), move(vs_out), move(perm), move(perm_inv));

}

SearchParam Midori(unsigned R) {
  vector<uint16_t> sbox = {0xc, 0xa, 0xd, 3, 0xe, 0xb, 0xf, 7,
                           8,   9,   1,   5, 0,   2,   4,   6}; // Midori

  vector<uint16_t> linear = {
    0b0001'0001'0001'0000,
    0b0010'0010'0010'0000,
    0b0100'0100'0100'0000,
    0b1000'1000'1000'0000,
    0b0001'0001'0000'0001,
    0b0010'0010'0000'0010,
    0b0100'0100'0000'0100,
    0b1000'1000'0000'1000,
    0b0001'0000'0001'0001,
    0b0010'0000'0010'0010,
    0b0100'0000'0100'0100,
    0b1000'0000'1000'1000,
    0b0000'0001'0001'0001,
    0b0000'0010'0010'0010,
    0b0000'0100'0100'0100,
    0b0000'1000'1000'1000
  };

  vector<uint8_t> perm_inv = {0,10,5,15,14,4,11,1,9,3,12,6,7,13,2,8}; // nibble i <-- nibble perm_inv[i]
  vector<uint8_t> perm (16); // nibble perm[i]  <-- nibble i
  for (unsigned i = 0; i < 16; ++i) perm[perm_inv[i]] = i;

  auto anf_s = getANF_Parallel_S(4, sbox);
  auto anf_l = convertLtoPolynome(linear);

  auto anf_los = composition(anf_l, anf_s);
  auto anf_kolos = anf_los;
  for (unsigned k = 0; k < 16; ++k) anf_kolos[k] += Monome(uint32_t(1) << (16 + k));
  auto anf_sokolos = composition(anf_s, anf_kolos);

  auto anf_sol = composition(anf_s, anf_l);

  auto const & dL = loadDivTable (anf_l);
  auto const & dSLKS = loadDivTable (anf_sokolos);


  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_in (4, loadTransitionsIN(anf_sokolos));
  cout << "Transitions IN (columns 0-3): " << endl;
  printTransitions(vs_in[0]);
  cout << " -------------------------- " << endl;

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_out (4, R%2 == 0 ? loadTransitionsOUT(anf_sokolos) : loadTransitionsOUT(anf_sol));
  cout << "Transitions OUT (columns 0-3): " << endl;
  printTransitions(vs_out[0]);
  cout << " -------------------------- " << endl;

  vector<DivTable> vSLKS (4, dSLKS);
  vector<DivTable> vL (4, dL);
  vector<vector<DivTable>> vdiv;
  R -= 2;
  while (R >= 3) {
    vdiv.emplace_back(vL);
    vdiv.emplace_back(vSLKS);
    R -= 2;
  }
  if (R == 2) vdiv.emplace_back(vL);

  return SearchParam(move(vdiv), move(vs_in), move(vs_out), move(perm), move(perm_inv));

}

SearchParam Craft(unsigned R) {
  vector<uint16_t> sbox = {0xc, 0xa, 0xd, 3, 0xe, 0xb, 0xf, 7,
                           8,   9,   1,   5, 0,   2,   4,   6}; // Midori

  vector<uint16_t> linear = {
                               0b0001'0001'0000'0001,
                               0b0010'0010'0000'0010,
                               0b0100'0100'0000'0100,
                               0b1000'1000'0000'1000,
                               0b0001'0000'0001'0000,
                               0b0010'0000'0010'0000,
                               0b0100'0000'0100'0000,
                               0b1000'0000'1000'0000,
                               0b0000'0001'0000'0000,
                               0b0000'0010'0000'0000,
                               0b0000'0100'0000'0000,
                               0b0000'1000'0000'0000,
                               0b0001'0000'0000'0000,
                               0b0010'0000'0000'0000,
                               0b0100'0000'0000'0000,
                               0b1000'0000'0000'0000
                             };

  vector<uint8_t> perm_inv = {15,10,9,4,3,6,5,8,7,2,1,12,11,14,13,0}; // nibble i <-- nibble perm_inv[i]
  vector<uint8_t> perm (16); // nibble perm[i]  <-- nibble i
  for (unsigned i = 0; i < 16; ++i) perm[perm_inv[i]] = i;

  auto anf_s = getANF_Parallel_S(4, sbox);
  auto anf_l = convertLtoPolynome(linear);

  auto anf_los = composition(anf_l, anf_s);
  auto anf_kolos = anf_los;
  for (unsigned k = 0; k < 16; ++k) anf_kolos[k] += Monome(uint32_t(1) << (16 + k));
  auto anf_sokolos = composition(anf_s, anf_kolos);

  auto anf_sol = composition(anf_s, anf_l);

  auto const & dL = loadDivTable (anf_l);
  auto const & dSLKS = loadDivTable (anf_sokolos);


  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_in (4, loadTransitionsIN(anf_sokolos));
  cout << "Transitions IN (columns 0-3): " << endl;
  printTransitions(vs_in[0]);
  cout << " -------------------------- " << endl;

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_out (4, R%2 == 0 ? loadTransitionsOUT(anf_sokolos) : loadTransitionsOUT(anf_sol));
  cout << "Transitions OUT (columns 0-3): " << endl;
  printTransitions(vs_out[0]);
  cout << " -------------------------- " << endl;

  vector<DivTable> vSLKS (4, dSLKS);
  vector<DivTable> vL (4, dL);
  vector<vector<DivTable>> vdiv;
  R -= 2;
  while (R >= 3) {
    vdiv.emplace_back(vL);
    vdiv.emplace_back(vSLKS);
    R -= 2;
  }
  if (R == 2) vdiv.emplace_back(vL);

  return SearchParam(move(vdiv), move(vs_in), move(vs_out), move(perm), move(perm_inv));

}


SearchParam Joltik(unsigned R) {
  vector<uint16_t> sbox = {14,4,11,2,3,8,0,9,1,10,7,15,6,12,5,13};

                          vector<vector<uint8_t>> val({{1,0,0,0,0,0,1,0,1,1,0,0,1,1,1,0},
                                          {0,1,0,0,0,0,1,1,0,0,1,0,0,0,0,1},
                                          {0,0,1,0,1,0,0,1,0,0,0,1,1,0,0,0},
                                          {0,0,0,1,0,1,0,0,1,0,0,0,1,1,0,0},
                                          {0,0,1,0,1,0,0,0,1,1,1,0,1,1,0,0},
                                          {0,0,1,1,0,1,0,0,0,0,0,1,0,0,1,0},
                                          {1,0,0,1,0,0,1,0,1,0,0,0,0,0,0,1},
                                          {0,1,0,0,0,0,0,1,1,1,0,0,1,0,0,0},
                                          {1,1,0,0,1,1,1,0,1,0,0,0,0,0,1,0},
                                          {0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,1},
                                          {0,0,0,1,1,0,0,0,0,0,1,0,1,0,0,1},
                                          {1,0,0,0,1,1,0,0,0,0,0,1,0,1,0,0},
                                          {1,1,1,0,1,1,0,0,0,0,1,0,1,0,0,0},
                                          {0,0,0,1,0,0,1,0,0,0,1,1,0,1,0,0},
                                          {1,0,0,0,0,0,0,1,1,0,0,1,0,0,1,0},
                                          {1,1,0,0,1,0,0,0,0,1,0,0,0,0,0,1}});

  vector<uint16_t> linear (16, 0);
  for (unsigned l = 0; l < 16; ++l) {
    for (unsigned b = 0; b < 16; ++b) linear[l] |= uint16_t(val[l][b]) << b;
  }


  vector<uint8_t> perm_inv = {0,10,5,15,14,4,11,1,9,3,12,6,7,13,2,8}; // nibble i <-- nibble perm_inv[i]
  vector<uint8_t> perm (16); // nibble perm[i]  <-- nibble i
  for (unsigned i = 0; i < 16; ++i) perm[perm_inv[i]] = i;

  auto anf_s = getANF_Parallel_S(4, sbox);
  auto anf_l = convertLtoPolynome(linear);

  auto anf_los = composition(anf_l, anf_s);
  auto anf_kolos = anf_los;
  for (unsigned k = 0; k < 16; ++k) anf_kolos[k] += Monome(uint32_t(1) << (16 + k));
  auto anf_sokolos = composition(anf_s, anf_kolos);

  auto anf_sol = composition(anf_s, anf_l);

  auto const & dL = loadDivTable (anf_l);
  auto const & dSLKS = loadDivTable (anf_sokolos);


  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_in (4, loadTransitionsIN(anf_sokolos));
  cout << "Transitions IN (columns 0-3): " << endl;
  printTransitions(vs_in[0]);
  cout << " -------------------------- " << endl;

  vector<vector<pair<vector<uint16_t>, unsigned>>> vs_out (4, R%2 == 0 ? loadTransitionsOUT(anf_sokolos) : loadTransitionsOUT(anf_sol));
  cout << "Transitions OUT (columns 0-3): " << endl;
  printTransitions(vs_out[0]);
  cout << " -------------------------- " << endl;

  vector<DivTable> vSLKS (4, dSLKS);
  vector<DivTable> vL (4, dL);
  vector<vector<DivTable>> vdiv;
  R -= 2;
  while (R >= 3) {
    vdiv.emplace_back(vL);
    vdiv.emplace_back(vSLKS);
    R -= 2;
  }
  if (R == 2) vdiv.emplace_back(vL);

  return SearchParam(move(vdiv), move(vs_in), move(vs_out), move(perm), move(perm_inv));

}
