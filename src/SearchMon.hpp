#ifndef SEARCHMON_HPP
#define SEARCHMON_HPP

#include "Polynome.hpp"
#include <map>
#include <set>

class SearchMon {
public:
  SearchMon(std::vector<uint8_t> p, std::vector<uint16_t> const &sbox,
            std::vector<std::vector<uint8_t>> const &m);

  void applyS(unsigned i);
  void applyK(unsigned b);
  void applyP();
  void applyL(unsigned i);
  void applyL2(unsigned i);

  void init(unsigned i) {
    maybe.clear();
    sure = std::vector<uint64_t>{~(uint64_t(1) << i)};
  };

  std::vector<uint64_t> sure;
  std::vector<uint64_t> maybe;

private:
  std::vector<uint8_t> perm;
  std::vector<Polynome> pol_sbox;
  std::vector<Polynome> pol_linear;

  unsigned s_size;
  unsigned m_size;

  std::map<std::pair<std::set<uint8_t>, std::set<uint8_t>>,
           std::pair<std::vector<uint8_t>, std::vector<uint8_t>>>
      table_s;
  std::map<std::pair<std::set<uint8_t>, std::set<uint8_t>>,
           std::pair<std::vector<uint8_t>, std::vector<uint8_t>>>
      table_m;
  std::map<std::pair<std::set<uint8_t>, std::set<uint8_t>>,
           std::pair<std::vector<uint8_t>, std::vector<uint8_t>>>
      table_k;
};

#endif
