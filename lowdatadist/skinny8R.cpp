#include <iostream>
#include <random>
#include <vector>
#include <execution>
#include <mutex>

using namespace std;

uint8_t sbox[16] = {0xc, 6, 9, 0, 1, 0xa, 2, 0xb, 3, 8, 5, 0xd, 4, 0xe, 7, 0xf};
uint8_t p[16] = {0, 5,  10, 15, 4,  9, 14, 3, 8, 13, 2,  7,  12, 1, 6,  11};

uint64_t SubNibbles(uint64_t x)
{
    uint64_t bit0 = ~x;
    uint64_t bit1 = bit0 >> 1;
    uint64_t bit2 = bit0 >> 2;
    uint64_t bit3 = bit0 >> 3;
    bit0 ^= bit3 & bit2;
    bit3 ^= bit1 & bit2;
    bit2 ^= bit1 & bit0;
    bit1 ^= bit0 & bit3;
    x = ((bit0 << 3) & 0x8888888888888888ULL) |
        ( bit1       & 0x1111111111111111ULL) |
        ((bit2 << 1) & 0x2222222222222222ULL) |
        ((bit3 << 2) & 0x4444444444444444ULL);
    return ~x;
}

uint64_t ShiftCells(uint64_t x) {
  uint64_t y = x & 0x000F000F000F000FULL;
  y |= (x << 16) & 0x00F000F000F00000ULL;
  y |= (x >> 48) & 0x00000000000000F0ULL;
  y |= (x << 32) & 0x0F000F0000000000ULL;
  y |= (x >> 32) & 0x000000000F000F00ULL;
  y |= (x << 48) & 0xF000000000000000ULL;
  y |= (x >> 16) & 0x0000F000F000F000ULL;
  return y;
}

uint64_t MixColumns(uint64_t x) {
  uint64_t const & line0 = x & 0x000F000F000F000FULL;
  uint64_t const & line1 = (x & 0x00F000F000F000F0ULL) >> 4;
  uint64_t const & line2 = (x & 0x0F000F000F000F00ULL) >> 8;
  uint64_t const & line3 = (x & 0xF000F000F000F000ULL) >> 12;

  uint64_t const & u3 = (line0 ^ line2);
  uint64_t y = (u3 ^ line3);
  y |= line0 << 4;
  y |= (line1 ^ line2) << 8;
  y |= u3 << 12;
  return y;
}

uint64_t Round(uint64_t x, uint64_t k) { // one round of SKINNY
  x = SubNibbles(x);
  x ^= k;
  x ^= uint64_t(2) << 8; // round constant (other part is masked by random key)
  x = ShiftCells(x);
  return MixColumns(x);
}

uint64_t FirstRound(uint64_t x, uint64_t k) { // round of SKINNY without SC (assume was performed before)
  x = SubNibbles(x);
  x ^= k;
  x ^= uint64_t(2) << (8+32); // round constant (other part is masked by random key)
  return MixColumns(x);
}

void test8R(unsigned N) {
  std::random_device rd;
  bool flag = true;
  unsigned cpt = 0;

  vector<uint64_t> vc0;
  for (uint64_t c0 = 0; c0 < 1u << 16; ++c0) {
    if ((c0 & 2) == 0) vc0.emplace_back(c0);
  }

  for (unsigned e = 0; e < N; ++e) {
    uint64_t k[7];
    uint64_t x;
    for (unsigned i = 0; i < 7; ++i) k[i] = (uint64_t(rd()) | (uint64_t(rd()) << 32)) & 0x00FF00FF00FF00FFULL;
    x = uint64_t(rd()) | (uint64_t(rd()) << 32);

    uint64_t res = 0;

    vector<uint64_t> tc0 = vc0;

    for_each(execution::par_unseq, tc0.begin(), tc0.end(), [&x, &k](uint64_t & c0){
      uint64_t y = FirstRound(x ^ c0, k[0]);
      for (unsigned i = 1; i < 7; ++i) y = Round(y, k[i]);
      c0 = SubNibbles(y);
    });

    for (auto const & tt : tc0) res ^= tt;

    if (((res >> 7) & 1) != 0) {
      cout << endl << "Distinguisher on 8R not working :/" << endl;
      flag = false;
    }
    else cout << "\r" << ++cpt << " success" << flush;
  }
  cout << endl;
  if (flag) cout << "Yes!! (8R)" << endl;
}



int main(int argc, char const *argv[]) {

  test8R(stoi(argv[1]));

  return 0;
}
