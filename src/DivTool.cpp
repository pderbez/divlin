#include <omp.h>
#include <execution>

#include "Polynome.hpp"
#include "Simon.hpp"
#include "Search.hpp"
#include "Ciphers.hpp"

using namespace std;

uint64_t f(unsigned k) {
  if (k == 0) return 1;
  return 2*f(k-1) + 3*(1u << (k)) + 2*k;
}

void printMessage() {
  cout << "First parameter has to be a cipher of the following list: " << endl;
  cout << " - skinny, midori, gift, present, twine, hight, led" << endl;
  cout << endl;
  cout << "Second parameter is the number of rounds" << endl;
  cout << endl;
  cout << "Third parameter (optionnal) fixes the number of SSBs set to 0 (to search for low data distinguishers)" << endl;
}

pair<SearchParam, unsigned> chosenCipher(string const & s, unsigned R) {
  if (s == "skinny") {
    return make_pair(Skinny(R), 4);
  }
  if (s == "midori") {
    return make_pair(Midori(R), 1);
  }
  if (s == "gift") {
    return make_pair(Gift(R), 4);
  }
  if (s == "present") {
    return make_pair(Present(R), 1);
  }
  if (s == "twine") {
    return make_pair(Twine(R), 4);
  }
  if (s == "hight") {
    return make_pair(Hight(R), 2);
  }
  if (s == "led") {
    return make_pair(Led(R), 1);
  }
  if (s == "craft") {
    return make_pair(Craft(R),1);
  }
  if (s == "joltik") {
    return make_pair(Joltik(R),1);
  }
  cout << "cipher not recorded" << endl;
  return make_pair(Skinny(3), 0);
}



int main(int argc, char const *argv[]) {

  if (argc < 2 || argc > 4) {
    printMessage();
    return 0;
  }

  auto p = chosenCipher(argv[1], stoi(argv[2]));
  if (p.second == 0) {
    printMessage();
    return 0;
  }
  auto & param = p.first;

  int nb0 = 0;
  if (argc == 4) {
    nb0 = stoi(argv[3]);
    if (nb0 < 0 || nb0 > 3) {
      printMessage();
      return 0;
    }
  }

  cout << "Removing unnecessary trails: " << flush;
  removeUnnecessaryTrails(param.vdiv, param.perm);
  cout << "done" << endl;

  cout << endl << "A: SSB fully active, 0: SSB constant, x: constant linear combinaison (eg, 544 means b5 + b9 constant)" << endl << endl;

  for (unsigned co = 0; co < p.second; ++co) {
    searchDistinguishers3(nb0, co, param.s_in, param.s_out[co], param.perm, param.perm_inv, param.vdiv);
  }


  return 0;
}
