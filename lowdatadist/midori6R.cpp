#include <iostream>
#include <random>
#include <vector>
#include <execution>
#include <mutex>

using namespace std;

uint8_t sbox[16] = {0xc, 0xa, 0xd, 3, 0xe, 0xb, 0xf, 7,  8,   9,   1,   5, 0,   2,   4,   6};
uint8_t p[16] = {0,10,5,15,14,4,11,1,9,3,12,6,7,13,2,8}; // nibble i <-- nibble perm_inv[i]

uint64_t SubNibbles(uint64_t x) {
  uint64_t y = 0;
  for (unsigned i = 0; i < 16; ++i) {
    y |= uint64_t(sbox[(x >> 4 * i) & 0xF]) << 4 * i;
  }
  return y;
}

uint64_t ShiftCells(uint64_t x) {
  uint64_t y = 0;
  for (unsigned i = 0; i < 16; ++i) {
    y |= uint64_t((x >> 4*p[i]) & 0xF) << 4*i;
  }
  return y;
}

uint64_t MixColumns(uint64_t x) {
  uint64_t const & line0 = x & 0x000F000F000F000FULL;
  uint64_t const & line1 = (x & 0x00F000F000F000F0ULL) >> 4;
  uint64_t const & line2 = (x & 0x0F000F000F000F00ULL) >> 8;
  uint64_t const & line3 = (x & 0xF000F000F000F000ULL) >> 12;

  uint64_t y = (line1 ^ line2 ^ line3);
  y |= (line0 ^ line2 ^ line3) << 4;
  y |= (line0 ^ line1 ^ line3) << 8;
  y |= (line0 ^ line1 ^ line2) << 12;
  return y;
}

uint64_t Round(uint64_t x, uint64_t k) { // one round of Midori
  x = SubNibbles(x);
  x = ShiftCells(x);
  return MixColumns(x)^k;
}

uint64_t FirstRound(uint64_t x, uint64_t k) { // round of Midori without SC (assume was performed before)
  x = SubNibbles(x);
  return MixColumns(x)^k;
}

void test6R(unsigned N) {
  std::random_device rd;
  bool flag = true;
  unsigned cpt = 0;

  vector<uint64_t> vc0;
  for (uint64_t c0 = 0; c0 < 1u << 16; ++c0) {
    if ((c0 & (1u << 9)) == 0) vc0.emplace_back(c0);
  }

  for (unsigned e = 0; e < N; ++e) {
    uint64_t k[5];
    uint64_t x;
    for (unsigned i = 0; i < 5; ++i) k[i] = (uint64_t(rd()) | (uint64_t(rd()) << 32));
    x = uint64_t(rd()) | (uint64_t(rd()) << 32);

    uint64_t res = 0;

    vector<uint64_t> tc0 = vc0;

    for_each(execution::par_unseq, tc0.begin(), tc0.end(), [&x, &k](uint64_t & c0){
      uint64_t y = FirstRound(x ^ c0, k[0]);
      for (unsigned i = 1; i < 5; ++i) y = Round(y, k[i]);
      c0 = SubNibbles(y);
    });

    for (auto const & tt : tc0) res ^= tt;

    uint8_t b = (res >> 1) & 1;
    b ^= (res >> 5) & 1;
    b ^= (res >> 9) & 1;
    b ^= (res >> 13) & 1;

    // uint8_t b = (res >> 0) & 1;
    // b ^= (res >> 1) & 1;
    // b ^= (res >> 2) & 1;
    // b ^= (res >> 4) & 1;
    // b ^= (res >> 5) & 1;
    // b ^= (res >> 6) & 1;
    // b ^= (res >> 8) & 1;
    // b ^= (res >> 9) & 1;
    // b ^= (res >> 10) & 1;
    // b ^= (res >> 12) & 1;
    // b ^= (res >> 13) & 1;
    // b ^= (res >> 14) & 1;

    if (b != 0) {
      cout << endl << "Distinguisher on 6R not working :/" << endl;
      flag = false;
    }
    else cout << "\r" << ++cpt << " success" << flush;
  }
  cout << endl;
  if (flag) cout << "Yes!! (6R)" << endl;
}



int main(int argc, char const *argv[]) {

  test6R(stoi(argv[1]));

  return 0;
}
