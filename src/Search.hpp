#ifndef SEARCH_HPP
#define SEARCH_HPP

#include <cstdint>
#include <vector>
#include "DivTable.hpp"

bool searchTrail(uint64_t in64, uint64_t out64, std::vector<std::vector<DivTable>> vdiv, std::vector<uint8_t> const & perm, std::vector<uint8_t> const & perm_inv);

void searchDistinguishers(unsigned ci, unsigned co, std::vector<std::pair<std::vector<uint16_t>, unsigned>> const & s_in,
   std::vector<std::pair<std::vector<uint16_t>, unsigned>> const & s_out, std::vector<uint8_t> const & perm, std::vector<uint8_t> const & perm_inv,
   std::vector<std::vector<DivTable>> const & vdiv);

void searchDistinguishers(unsigned n0sbox, unsigned ci, unsigned co, std::vector<std::pair<std::vector<uint16_t>, unsigned>> const & s_in,
  std::vector<std::pair<std::vector<uint16_t>, unsigned>> const & s_out, std::vector<uint8_t> const & perm, std::vector<uint8_t> const & perm_inv,
  std::vector<std::vector<DivTable>> const & vdiv);

void searchDistinguishers2(unsigned n0sbox, unsigned ci, unsigned co, std::vector<std::pair<std::vector<uint16_t>, unsigned>> const & s_in,
  std::vector<std::pair<std::vector<uint16_t>, unsigned>> const & s_out, std::vector<uint8_t> const & perm, std::vector<uint8_t> const & perm_inv,
  std::vector<std::vector<DivTable>> const & vdiv);

void searchDistinguishers3(int n0sbox, unsigned co, std::vector<std::vector<std::pair<std::vector<uint16_t>, unsigned>>> const & s_in,
  std::vector<std::pair<std::vector<uint16_t>, unsigned>> const & s_out, std::vector<uint8_t> const & perm, std::vector<uint8_t> const & perm_inv,
  std::vector<std::vector<DivTable>> const & vdiv);

void searchDistinguishers4(int n0sbox, unsigned co, std::vector<std::vector<std::pair<std::vector<uint16_t>, unsigned>>> const & s_in,
  std::vector<std::pair<std::vector<uint16_t>, unsigned>> const & s_out, std::vector<uint8_t> const & perm, std::vector<uint8_t> const & perm_inv,
  std::vector<std::vector<DivTable>> const & vdiv);


#endif
